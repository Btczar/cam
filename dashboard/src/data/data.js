// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Is used to get calculate / derive the data to display from the received data via REST

import * as REST from '../data/REST'

// General configuration information
export function getAllServices() {
    return REST.getAllServices();
}

export function getAllControls() {
    return REST.getAllControls();
}

export function newService(name) {
    REST.saveService(name);
}

export function saveConfig(config) {
    REST.saveConfig(config);
}

// Evaluations
export function getEval() {
    return REST.getEvaluation();
}

export function getHistory() {
    return REST.getHistory();
}

export function getEvidence(id) {
    return REST.getEvidence(id);
}

// Calculated values for evaluation
export function getNumberofServices() {
    return getEval().length;
}

export function getNumberofCompliantServices() {
    var counter = 0;
    getEval().forEach((service) => {
        var allCompl = true
        service.controls.forEach((control) => {
            if (!control.controlRes) allCompl = false;
        });
        if (allCompl) counter++;
    });
    return counter;
}


// Calculated values for history (last 30 days)

// Returns an array with the number of compliant services per day
export function getCompPerDay() {
    var comp = [];
    getHistory().forEach((day) => {
        var daycounter = 0;
        day.data.forEach((service) => {
            var allCompl = true
            service.controls.forEach((control) => {
                if (!control.controlRes) allCompl = false;
            });
            if (allCompl) daycounter++;
        });
        comp.push(daycounter);
    });

    // fill up with 0 for days without data
    for (var i = getHistory().length; i <= 30; i++) {
        comp.push(0);
    }
    return comp;
}

export function getDaysCompliant() {
    var compcounter = 0;
    getHistory().forEach((day) => {
        var servicecounter = 0;
        day.data.forEach((service) => {
            var allCompl = true
            service.controls.forEach((control) => {
                if (!control.controlRes) allCompl = false;
            });
            if (allCompl) servicecounter++;
        });
        if (servicecounter == day.data.length) compcounter++;
    });
    return compcounter;
}

// Counts the compliance of each service over all days
export function countCompliancePerService() {
    var counter = [];
    var comp = [];
    getHistory().forEach((day) => {
        var daycounter = 0;
        day.data.forEach((service) => {
            var allCompl = true
            service.controls.forEach((control) => {
                if (!control.controlRes) allCompl = false;
            });
            if (allCompl) {
                if (counter.some(s => s.id === service.serviceId)) {
                    // update counter
                    counter.find(s => s.id === service.serviceId).count++;
                } else {
                    // add to counter
                    var tmp = new Object;
                    tmp.name = service.serviceName;
                    tmp.id = service.serviceId;
                    tmp.count = 1;
                    counter.push(tmp);
                }
            }
        });
        comp.push(daycounter);
    });

    return counter;
}

export function getMostCompliantService() {
    var result = "";
    // find n max
    var max = -1;
    countCompliancePerService().forEach((service) => {
        if (service.count > max) max = service.count;
    });

    // add servicename to result
    countCompliancePerService().forEach((service) => {
        if (service.count == max) {
            if (result == "") {
                result = result + service.name;
            } else {
                result = result + ", " + service.name;
            }

        }
    });
    return result;
}

export function getLeastCompliantService() {
    var result = "";
    // find n min
    var min = 1000000;
    countCompliancePerService().forEach((service) => {
        if (service.count < min) min = service.count;
    });

    // add servicename to result
    countCompliancePerService().forEach((service) => {
        if (service.count == min) {
            if (result == "") {
                result = result + service.name;
            } else {
                result = result + ", " + service.name;
            }

        }
    });
    return result;
}

export function getEvalForDay(day) {
    return getHistory().find(e => (e.day == day)).data;
}

// Settings
export function getSetting(key) {
    REST.getSetting(key);
}

export function setSetting(key, value) {
    REST.setSetting(key, value);
}

export function setOpenStack(config) {
    //TODO: REST
}

export function setSKubernetes(file) {
    //TODO: REST
}








