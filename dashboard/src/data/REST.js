// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Is used to communicate with the API

//******************/
// Set this to use testdata if REST is not available
var testdata = false
//******************/

// Is used to get the data via REST
const URL = "/v1/";

// Get live evaluation data
export function getEvaluation() {
    if (!testdata) {
        // Get service Ids
        let ids = getAllServices().services;
        // Get evaluation for each service
        var evalRes = [];
        ids.forEach(element => {
            var tmp = [
                {
                    serviceId: element.id,
                    serviceName: "Service1",
                    controls: getDataFromApi("evaluation/cloud_services/" + element.id + "/compliance")
                }];
            res.push(tmp);
        });
        return evalRes;
    } else {
        return evalResTest;
    }
}

// Get evaluation data of last 30 days
export function getHistory() {
    if (!testdata) {
        // Get service Ids
        let ids = getAllServices().services;
        // Get evaluation for each service
        var evalRes = [];
        ids.forEach(element => {
            var tmp = [
                {
                    serviceId: element.id,
                    serviceName: "Service1",
                    controls: getDataFromApi("evaluation/cloud_services/" + element.id + "/compliance?days=30")
                }];
            evalRes.push(tmp);
        });
        return evalRes;
    } else {
        return evalResTest30;
    }
}

// Get lists of ALL services / controls
export function getAllServices() {
    if (!testdata) {
        return getDataFromApi("configuration/cloud_services");
    } else {
        return Promise.resolve({ "services": [{ name: 'Service-1' }, { name: 'Service-2' }, { name: 'Service-3' }, { name: 'Service-4' }] });
    }
}

export function getAllControls() {
    if (!testdata) {
        return getDataFromApi("configuration/controls");
    } else {
        return Promise.resolve({ "requirements": [{ name: 'Control-A' }, { name: 'Control-C' }] });
    }
}

// Set services to monitor
export function setMonitoring(settings) {
    if (!testdata) {
        settings.forEach(element => {
            data = { "id": element.controlId };
            var path = "configuration/cloud_services/" + element.serviceId + "/controls/" + element.controlId;
            setDataToApi(path, data);
        });
    } else {
        alert("Monitoring configured");
    }
}

// Save new service
export function saveService(name) {
    if (!testdata) {
        var data = {
            "name": name,
        }
        setDataToApi("configuration/cloud_services", data)
    } else {
        alert("Service saved");
    }
}

export function getEvidence(id) {
    if (!testdata) {
        return getDataFromApi("evaluation/evidences/" + id);
    } else {
        return ("Evidence");
    }
}

// Settings

export function getSetting(key) {
    if (!testdata) {
        //TODO: API
        const tmp = getDataFromApi("??/" + key);
        // restructuring data?
        return tmp;
    } else {
        return "ValueA";
    }
}

export function setSetting(key, value) {
    if (!testdata) {
        var data = {
            "key": key,
            "value": value,
        }
        setDataToApi("configuration/cloud_services", data)
    } else {
        alert("Setting " + key + " set");
    }
}

// Is used to send to REST API
async function getDataFromApi(path) {
    return fetch(URL + path)
        .then((resp) => resp.json())
        .catch((error) => {
            console.log(error);
        });
}

async function setDataToApi(path, data) {
    var request = new Request(URL + path, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers()
    });
    fetch(request)
        .then(function () {
            //
        })
        .catch(function (error) {
            console.log(error);
        });
}


/***************TESTDATA***************/
// Testdata for evaluation with 4 services
const evalResTest = [
    {
        serviceId: "1",
        serviceName: "Service1",
        controls: [
            {
                controlId: "1",
                controlName: "Control1",
                controlRes: false,
                controlMessage: "not ok",
            },
            {
                controlId: "2",
                controlName: "Control2",
                controlRes: true,
                controlMessage: "ok",
            }
        ]
    },
    {
        serviceId: "2",
        serviceName: "Service2",
        controls: [
            {
                controlId: "1",
                controlName: "Control1",
                controlRes: false,
                controlMessage: "not ok",
            },
            {
                controlId: "2",
                controlName: "Control2",
                controlRes: true,
                controlMessage: "ok",
            },
            {
                controlId: "3",
                controlName: "Control3",
                controlRes: true,
                controlMessage: "ok",
            }
        ]
    },
    {
        serviceId: "3",
        serviceName: "Service3",
        controls: [
            {
                controlId: "1",
                controlName: "Control1",
                controlRes: false,
                controlMessage: "not ok",
            },
            {
                controlId: "2",
                controlName: "Control2",
                controlRes: true,
                controlMessage: "ok",
            }
        ]
    },
    {
        serviceId: "4",
        serviceName: "Service4",
        controls: [
            {
                controlId: "1",
                controlName: "Control1",
                controlRes: true,
                controlMessage: "ok",
            },
            {
                controlId: "2",
                controlName: "Control2",
                controlRes: true,
                controlMessage: "ok",
            }
        ]
    }
];

// Testdata for 30 last days 
const evalResTest30 = [
    {
        day: 1,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 2,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 3,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 4,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 5,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 6,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 7,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 8,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 9,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 10,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 11,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 12,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 13,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 14,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 15,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 16,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 17,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 18,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 19,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 20,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 21,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 22,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 23,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 24,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 25,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 26,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 27,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }, {
        day: 28,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 29,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: false,
                        controlMessage: "not ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    },
    {
        day: 30,
        data: [
            {
                serviceId: "1",
                serviceName: "Service1",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "2",
                serviceName: "Service2",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "3",
                        controlName: "Control3",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "3",
                serviceName: "Service3",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            },
            {
                serviceId: "4",
                serviceName: "Service4",
                controls: [
                    {
                        controlId: "1",
                        controlName: "Control1",
                        controlRes: true,
                        controlMessage: "ok",
                    },
                    {
                        controlId: "2",
                        controlName: "Control2",
                        controlRes: true,
                        controlMessage: "ok",
                    }
                ]
            }
        ]
    }];

