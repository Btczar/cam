// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Defines the routes of the page and the corresponding views
import { createRouter, createWebHashHistory } from 'vue-router'
import Settings from '../views/Settings.vue'
import Evaluation from '../views/Evaluation.vue'
import Home from '../views/Home.vue'
import Configuration from '../views/Configuration.vue'
import History from '../views/History.vue'

const router = createRouter({
  history: createWebHashHistory(),
    routes: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/settings',
        name: 'Settings',
        component: Settings
      },
      {
        path: '/evaluation',
        name: 'Evaluation',
        component: Evaluation
      },
      {
        path: '/configuration',
        name: 'Configuration',
        component: Configuration
      },
      {
        path: '/history',
        name: 'History',
        component: History
      }
    ]
  })
  
  export default router