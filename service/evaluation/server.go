// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package evaluation

import (
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"

	cl_api_assessment "clouditor.io/clouditor/api/assessment"
	cl_api_evidence "clouditor.io/clouditor/api/evidence"
	"clouditor.io/clouditor/api/orchestrator"
	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/persistence/gorm"
	"clouditor.io/clouditor/policies"
	cl_service "clouditor.io/clouditor/service"
	cl_service_assessment "clouditor.io/clouditor/service/assessment"
	"github.com/go-co-op/gocron"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	log = logrus.WithField("component", "evaluation")

	DefaultRequirementsManagerAddress = grpcTarget{target: "127.0.0.1:50100"}
)

type grpcTarget struct {
	target string
	opts   []grpc.DialOption
}

type server struct {
	evaluation.UnimplementedEvaluationServer

	// Clouditor's assessment service
	*cl_service_assessment.Service

	// reqManagerAddress is the gRPC address used for the connection to the requirements manager
	reqManagerAddress grpcTarget

	// Storage of evidences. Will be a DB in the future
	evidences      map[string]*common.Evidence
	evidencesMutex sync.RWMutex

	requirementsSource policies.RequirementsSource

	// storage is our persistence backend
	storage persistence.Storage
}

// WithRequirementsManagerAddress is a server option for setting the address of the Requirements Manager
func WithRequirementsManagerAddress(address string, opts ...grpc.DialOption) service.ServiceOption[server] {
	return func(srv *server) {
		srv.reqManagerAddress = grpcTarget{
			target: address,
			opts:   opts,
		}
	}
}

// WithStorage is an option to set the storage. If not set, NewServer will use inmemory storage.
func WithStorage(storage persistence.Storage) service.ServiceOption[server] {
	return func(srv *server) {
		srv.storage = storage
	}
}

// NewServer creates a new evaluation server/service
func NewServer(opts ...service.ServiceOption[server]) (srv *server) {
	var err error

	srv = &server{
		reqManagerAddress: DefaultRequirementsManagerAddress,
		evidences:         make(map[string]*common.Evidence),
	}

	// Apply any options
	for _, o := range opts {
		o(srv)
	}

	// Initialize in-memory storage backend if storage is not set already
	if srv.storage == nil {
		srv.storage, err = gorm.NewStorage(
			gorm.WithAdditionalAutoMigration(evaluation.EvaluationResult{}, evaluation.Compliance{}),
			gorm.WithInMemory(),
			gorm.WithMaxOpenConns(1), // Otherwise race conditions might occur due to in-memory option (= sqlite)
		)

		if err != nil {
			log.Errorf("Could not initialize storage. Configuration server will probably not function correctly: %v", err)
		}
	}

	// Build a new Clouditor assessment service. It takes care of the actual assessment of our policies.
	// It also fetches the needed metrics and their implementations automatically from the Requirements Manager,
	// since the Requirements Manager implements the Clouditor Orchestrator interface.
	srv.Service = cl_service_assessment.NewService(
		cl_service_assessment.WithOrchestratorAddress(srv.reqManagerAddress.target, srv.reqManagerAddress.opts...),
		cl_service_assessment.WithRegoPackageName("gxfs.metrics"),
		cl_service_assessment.WithoutEvidenceStore())

	srv.Service.RegisterAssessmentResultHook(srv.createEvaluationResult)

	srv.requirementsSource = CachedRequirementsSource(srv.Service)

	return
}

// ScheduleComplianceCheck is a temporary function to start the calculcation of compliance. It will later
// replaced with information coming from an API call.
func (s *server) ScheduleComplianceCheck(serviceID, controlID string) {
	scheduler := gocron.NewScheduler(time.UTC)
	_, err := scheduler.Every(5).Minute().Do(s.calculateCompliance, serviceID, controlID)
	if err != nil {
		log.Errorf("Could not start scheduler for `calculateCompliance` (control %s, service %s): %v",
			controlID, serviceID, err)
		return
	}

	// Start asynchronously. Otherwise, WithCalculatingCompliance would block NewServer
	scheduler.StartAsync()
}

// SendEvidences stores and evaluates evidences sent by a SendEvidencesClient via a stream
func (s *server) SendEvidences(stream evaluation.Evaluation_SendEvidencesServer) (err error) {
	var (
		// Evidence in CAM format
		evidence *common.Evidence
		// Evidence in Clouditor format
		clouditorEvidence *cl_api_evidence.Evidence
		// Evidence including error is stored but not evaluated
		isEvidenceWithError = false
	)

	// Loop through stream for receiving evidences
	for {
		evidence, err = stream.Recv()
		// If error represents end of file, log it and return
		if errors.Is(err, io.EOF) {
			log.Errorf("Received final input: %v", err)
			return nil
		}
		// If another (connection) error occurs, log it and return
		if err != nil {
			err = fmt.Errorf("cannot receive stream request: %v", err)
			log.Error(err)
			// Transform error into gRPC error and return it
			err = status.Error(codes.Unknown, err.Error())
			return
		}

		log.Infof("Received evidence %s from collection module %s", evidence.Id, evidence.ToolId)

		// Validate evidence
		// TODO(lebogg): Directly return since it is likely that the following evidences will also be invalid
		if err = evidence.Validate(); err != nil {
			if errors.Is(err, common.ErrEvidenceWithError) {
				log.Info("Error contains error and, thus, is not evaluated.")
				isEvidenceWithError = true
			} else {
				log.Errorf("Evidence is not valid: %v", err)
				continue
			}
		}

		// Use `AssessEvidence` of Clouditor's assessment service to evaluate the evidence
		// First we transform a CAM evidence into a Clouditor evidence
		clouditorEvidence = &cl_api_evidence.Evidence{
			Id:        evidence.Id,
			Timestamp: evidence.GatheredAt,
			ServiceId: evidence.TargetService,
			ToolId:    evidence.ToolId,
			Raw:       evidence.RawEvidence,
			Resource:  evidence.Value,
		}

		err = s.storeEvidence(evidence)
		log.Tracef("Stored evidence: %v", evidence)
		if err != nil {
			err = fmt.Errorf("couldn't store evidence: %w", err)
			log.Error(err)
			continue
		}

		// Evidence that includes error won't be evaluated
		if isEvidenceWithError {
			isEvidenceWithError = false
			continue
		}

		// Use transformed evidence to evaluate evidence ("to assess" in Clouditor terminology).
		// We discard response since it is used in the streaming case `AssessEvidences`
		_, err = s.Service.AssessEvidence(
			context.TODO(),
			&cl_api_assessment.AssessEvidenceRequest{Evidence: clouditorEvidence})
		// Log error and continue since we still want to evaluate new incoming evidences
		if err != nil {
			err = fmt.Errorf("couldn't evaluate evidence: %w", err)
			log.Error(err)
			continue
		}
		log.Infof("Assessed new evidence")
	}
}

// GetEvaluation returns the most recent evaluation result for given service and metric IDs
func (s *server) GetEvaluation(_ context.Context, request *evaluation.GetEvaluationRequest) (eval *evaluation.EvaluationResult, err error) {
	var list []*evaluation.EvaluationResult

	// We need to use List because Get does not have any ordering. We limit the
	// list to 1 (which is essentially what Get does anyway)
	err = s.storage.List(&list, "time", false, 0, 1, "metric_id = ? AND service_id = ?", request.MetricId, request.ServiceId)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "database error: %v", err)
	}

	if len(list) == 0 {
		return nil, status.Error(codes.NotFound, "evaluation result not found")
	}

	eval = list[0]

	return
}

// GetCompliance returns the most recent compliance result for a particular control of a service
func (s *server) GetCompliance(_ context.Context, request *evaluation.GetComplianceRequest) (compliance *evaluation.Compliance, err error) {
	var list []*evaluation.Compliance

	// We need to use List because Get does not have any ordering. We limit the
	// list to 1 (which is essentially what Get does anyway)
	err = s.storage.List(&list, "time", false, 0, 1, "service_id = ?", request.ServiceId)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "database error: %v", err)
	}

	if len(list) == 0 {
		return nil, status.Error(codes.NotFound, "compliance result not found")
	}

	compliance = list[0]

	return
}

// ListCompliance returns all current compliance results for a particular service
func (s *server) ListCompliance(_ context.Context, request *evaluation.ListComplianceRequest) (res *evaluation.ListComplianceResponse, err error) {
	res = new(evaluation.ListComplianceResponse)

	res.ComplianceResults, res.NextPageToken, err = cl_service.PaginateStorage[*evaluation.Compliance](request, s.storage, cl_service.DefaultPaginationOpts, "service_id = ?", request.ServiceId)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "database error: %v", err)
	}

	return
}

// storeEvidence stores evidence to the in-memory map
func (s *server) storeEvidence(evidence *common.Evidence) error {
	s.evidencesMutex.Lock()
	s.evidences[evidence.Id] = evidence
	s.evidencesMutex.Unlock()
	log.Infof("New evidence for metric '%s': %s", evidence.GatheredUsing, evidence.Id)
	return nil
}

// calculateCompliance by checking the status of each evaluation result in newestResults
// TODO(lebogg): Execute it regularly or only via API calls?
func (s *server) calculateCompliance(serviceID, controlID string) (compliance *evaluation.Compliance, err error) {
	var requirements []*orchestrator.Requirement
	var control *orchestrator.Requirement

	// Start with compliant case. Non-compliant, if at least one result status is false
	compliance = &evaluation.Compliance{
		Id:          uuid.NewString(),
		ControlId:   controlID,
		Evaluations: nil,
		Status:      true,
		Time:        timestamppb.Now(),
	}

	// Fetch requirements (controls) from our source
	requirements, err = s.requirementsSource.Requirements()
	if err != nil {
		return nil, fmt.Errorf("could not fetch requirements: %w", err)
	}

	for _, r := range requirements {
		if r.Id == controlID {
			control = r
			break
		}
	}

	if control == nil {
		return nil, fmt.Errorf("control not found")
	}

	// For each metric (of this control), get latest evaluation result and calculate the compliance
	for _, m := range control.Metrics {
		var result []*evaluation.EvaluationResult
		err = s.storage.List(&result, "time", false, 0, 1, "metric_id = ? AND service_id = ?", m.Id, serviceID)
		if err != nil {
			return nil, fmt.Errorf("error while retrieving evaluation result from storage: %w", err)
		}

		// There is no result (currently) for this metric -> continue with next one
		if len(result) == 0 {
			continue
		}

		// If one result status is false -> non-compliant
		if !result[0].Status {
			compliance.Status = false
		}
		compliance.Evaluations = append(compliance.Evaluations, result[0])
	}

	// Save it
	err = s.storage.Save(compliance)
	if err != nil {
		return nil, fmt.Errorf("database error: %w", err)
	}

	return
}

// createEvaluationResult creates a GXFS evaluation result out of a Clouditor
// assessment result. In the future, we might merge the two concepts, but for
// now unfortunately, we have to do it like this.
func (srv *server) createEvaluationResult(result *cl_api_assessment.AssessmentResult, err error) {
	var (
		evidence *common.Evidence
		ok       bool
	)

	// Catch potential error
	if err != nil {
		log.Errorf("Can not create evaluation result: %s", err.Error())
		return
	}

	// Fetch evidence
	srv.evidencesMutex.RLock()
	evidence, ok = srv.evidences[result.EvidenceId]
	srv.evidencesMutex.RUnlock()
	if !ok {
		log.Errorf("Could not fetch evidence %s for assessment result", result.EvidenceId)
		return
	}

	eval := &evaluation.EvaluationResult{
		Id:         uuid.NewString(),
		MetricId:   evidence.GatheredUsing,
		ServiceId:  evidence.TargetService,
		EvidenceId: evidence.Id,
		Status:     result.Compliant,
		Time:       timestamppb.Now(),
	}

	err = srv.storage.Create(&eval)
	log.Tracef("Stored evaluation result: %v", eval)
	if err != nil {
		log.Errorf("Could not save result into database: %v", err)
		return
	}
}

type cachedRequirementsSource struct {
	src          policies.RequirementsSource
	requirements []*orchestrator.Requirement
}

func (c *cachedRequirementsSource) Requirements() (requirements []*orchestrator.Requirement, err error) {
	if c.requirements == nil {
		requirements, err = c.src.Requirements()
		if err != nil {
			c.requirements = requirements
		}

		return
	}

	return c.requirements, nil
}

func CachedRequirementsSource(src policies.RequirementsSource) policies.RequirementsSource {
	return &cachedRequirementsSource{
		src: src,
	}
}
