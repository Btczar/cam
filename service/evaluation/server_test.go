package evaluation

import (
	"context"
	"reflect"
	"testing"
	"time"

	"clouditor.io/clouditor/api/assessment"
	"clouditor.io/clouditor/api/orchestrator"
	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/policies"
	cl_service_assessment "clouditor.io/clouditor/service/assessment"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/testutil"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type staticRequirementsSource struct {
	src          policies.RequirementsSource
	requirements []*orchestrator.Requirement
}

func (s *staticRequirementsSource) Requirements() (requirements []*orchestrator.Requirement, err error) {
	return s.requirements, nil
}

func StaticRequirementsSource(requirements []*orchestrator.Requirement) policies.RequirementsSource {
	return &staticRequirementsSource{
		requirements: requirements,
	}
}

var TestRequirementsSource = StaticRequirementsSource([]*orchestrator.Requirement{
	{
		Id: "Control1",
		Metrics: []*assessment.Metric{
			{Id: "Metric1"},
			{Id: "Metric2"},
		},
	},
})

func Test_server_GetEvaluation(t *testing.T) {
	type fields struct {
		Service            *cl_service_assessment.Service
		evidences          map[string]*common.Evidence
		requirementsSource policies.RequirementsSource
		storage            persistence.Storage
	}
	type args struct {
		in0     context.Context
		request *evaluation.GetEvaluationRequest
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantEval *evaluation.EvaluationResult
		wantErr  bool
	}{
		{
			name: "latest evaluation result",
			fields: fields{
				storage: testutil.NewInMemoryStorage(t, func(s persistence.Storage) {
					s.Save(&evaluation.EvaluationResult{
						Id:        "00000000-0000-0000-0000-000000000000",
						MetricId:  "Metric1",
						ServiceId: "MyService",
						Time:      timestamppb.New(time.Date(2022, 1, 1, 1, 1, 1, 1, time.UTC)),
					})
					s.Save(&evaluation.EvaluationResult{
						Id:        "11111111-1111-1111-1111-111111111111",
						MetricId:  "Metric1",
						ServiceId: "MyService",
						Time:      timestamppb.New(time.Date(2022, 2, 1, 1, 1, 1, 1, time.UTC)),
					})
				}),
			},
			args: args{request: &evaluation.GetEvaluationRequest{
				ServiceId: "MyService",
				MetricId:  "Metric1",
			}},
			wantEval: &evaluation.EvaluationResult{
				Id:        "11111111-1111-1111-1111-111111111111",
				MetricId:  "Metric1",
				ServiceId: "MyService",
				Time:      timestamppb.New(time.Date(2022, 2, 1, 1, 1, 1, 1, time.UTC)),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &server{
				Service:            tt.fields.Service,
				evidences:          tt.fields.evidences,
				requirementsSource: tt.fields.requirementsSource,
				storage:            tt.fields.storage,
			}
			gotEval, err := s.GetEvaluation(tt.args.in0, tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("server.GetEvaluation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotEval, tt.wantEval) {
				t.Errorf("server.GetEvaluation() = %v, want %v", gotEval, tt.wantEval)
			}
		})
	}
}

func Test_server_calculateCompliance(t *testing.T) {
	type fields struct {
		Service            *cl_service_assessment.Service
		evidences          map[string]*common.Evidence
		requirementsSource policies.RequirementsSource
		storage            persistence.Storage
	}
	type args struct {
		serviceID string
		controlID string
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantCompliance assert.ValueAssertionFunc
		wantErr        bool
	}{
		{
			name: "No results",
			fields: fields{
				requirementsSource: TestRequirementsSource,
				storage:            testutil.NewInMemoryStorage(t),
			},
			args: args{
				serviceID: "MyService",
				controlID: "Control1",
			},
			wantCompliance: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				c, ok := i1.(*evaluation.Compliance)
				if !ok {
					return false
				}

				if !assert.Equal(t, 0, len(c.Evaluations)) {
					return false
				}
				if !assert.True(t, c.Status) {
					return false
				}
				if !assert.Equal(t, "Control1", c.ControlId) {
					return false
				}

				return true
			},
		},
		{
			name: "Latest results compliant",
			fields: fields{
				requirementsSource: TestRequirementsSource,
				storage: testutil.NewInMemoryStorage(t, func(s persistence.Storage) {
					// This is an old result, which is not relevant for the current state
					_ = s.Save(&evaluation.EvaluationResult{
						Id:        uuid.NewString(),
						ServiceId: "MyService",
						MetricId:  "Metric1",
						Status:    false,
						Time:      timestamppb.New(time.Now().Add(-10 * time.Minute)),
					})
					_ = s.Save(&evaluation.EvaluationResult{
						Id:        uuid.NewString(),
						ServiceId: "MyService",
						MetricId:  "Metric1",
						Status:    true,
						Time:      timestamppb.Now(),
					})
					_ = s.Save(&evaluation.EvaluationResult{
						Id:        uuid.NewString(),
						ServiceId: "MyService",
						MetricId:  "Metric2",
						Status:    true,
						Time:      timestamppb.Now(),
					})
				}),
			},
			args: args{
				serviceID: "MyService",
				controlID: "Control1",
			},
			wantCompliance: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				c, ok := i1.(*evaluation.Compliance)
				if !ok {
					return false
				}

				if !assert.Equal(t, 2, len(c.Evaluations)) {
					return false
				}
				if !assert.True(t, c.Status) {
					return false
				}
				if !assert.Equal(t, "Control1", c.ControlId) {
					return false
				}

				return true
			},
		},
		{
			name: "Latest results non-compliant",
			fields: fields{
				requirementsSource: TestRequirementsSource,
				storage: testutil.NewInMemoryStorage(t, func(s persistence.Storage) {
					_ = s.Save(&evaluation.EvaluationResult{
						Id:        uuid.NewString(),
						ServiceId: "MyService",
						MetricId:  "Metric1",
						Status:    false,
						Time:      timestamppb.New(time.Now().Add(-10 * time.Minute)),
					})
				}),
			},
			args: args{
				serviceID: "MyService",
				controlID: "Control1",
			},
			wantCompliance: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				c, ok := i1.(*evaluation.Compliance)
				if !ok {
					return false
				}

				if !assert.Equal(t, 1, len(c.Evaluations)) {
					return false
				}
				if !assert.False(t, c.Status) {
					return false
				}
				if !assert.Equal(t, "Control1", c.ControlId) {
					return false
				}

				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &server{
				Service:            tt.fields.Service,
				evidences:          tt.fields.evidences,
				requirementsSource: tt.fields.requirementsSource,
				storage:            tt.fields.storage,
			}

			gotCompliance, err := s.calculateCompliance(tt.args.serviceID, tt.args.controlID)
			if (err != nil) != tt.wantErr {
				t.Errorf("server.calculateCompliance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantCompliance != nil && !tt.wantCompliance(t, gotCompliance) {
				t.Errorf("server.calculateCompliance() = %v, want %v", gotCompliance, tt.wantCompliance)
			}
		})
	}
}
