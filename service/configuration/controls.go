// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"clouditor.io/clouditor/api/orchestrator"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/oscal"
)

// ListControls is a wrapper around Clouditor orchestrator
func (s *server) ListControls(ctx context.Context, req *orchestrator.ListRequirementsRequest) (*orchestrator.ListRequirementsResponse, error) {
	return s.OrchestratorServer.ListRequirements(ctx, req)
}

// loadRequirements loads requirements (or in Gaia-X speach "controls").
func loadRequirements() (requirements []*orchestrator.Requirement, err error) {
	var (
		b    []byte
		file = "gxfs.json"
	)

	var outer struct {
		Catalog oscal.Catalog `json:"catalog"`
	}

	log.Infof("Loading OSCAL catalog from %s", file)

	b, err = os.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("error while loading %s: %w", file, err)
	}

	err = json.Unmarshal(b, &outer)
	if err != nil {
		return nil, fmt.Errorf("error in JSON marshal: %w", err)
	}

	// Loop through controls
	for _, control := range outer.Catalog.Controls {
		// And create a requirement for it
		var r = orchestrator.Requirement{
			Id:       control.ID,
			Name:     control.Title,
			Metrics:  metricFor(&control),
			Category: "EUCS",
		}

		requirements = append(requirements, &r)
	}

	return requirements, nil
}
