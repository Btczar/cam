// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"
	"reflect"
	"testing"

	"clouditor.io/clouditor/api/assessment"
	"clouditor.io/clouditor/api/orchestrator"
	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/persistence/gorm"
	service_orchestrator "clouditor.io/clouditor/service/orchestrator"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm/logger"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"
)

func Test_server_StartMonitoring(t *testing.T) {
	storage, err := gorm.NewStorage(
		gorm.WithInMemory(),
		gorm.WithAdditionalAutoMigration(collection.CollectionModule{}),
		gorm.WithLogger(logger.Default.LogMode(logger.Info)))
	assert.NoError(t, err)

	// Populate some data
	err = storage.Save(&orchestrator.Requirement{Id: "Req-1", Metrics: []*assessment.Metric{
		{Id: "Metric-1"},
		{Id: "Metric-2"},
	}})
	assert.NoError(t, err)

	// Populate some data
	err = storage.Save(&collection.CollectionModule{Id: "Module-1", MetricId: "Metric-1"})
	assert.NoError(t, err)
	err = storage.Save(&collection.CollectionModule{Id: "Module-2", MetricId: "Metric-2"})
	assert.NoError(t, err)

	type fields struct {
		OrchestratorService orchestrator.OrchestratorServer
		storage             persistence.Storage
	}
	type args struct {
		ctx context.Context
		req *configuration.StartMonitoringRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantRes *configuration.StartMonitoringResponse
		wantErr bool
	}{
		{
			name: "Good",
			fields: fields{
				OrchestratorService: service_orchestrator.NewService(service_orchestrator.WithStorage(storage)),
				storage:             storage,
			},
			args: args{
				req: &configuration.StartMonitoringRequest{ControlIds: []string{"Req-1"}, ServiceId: "myService"},
			},
			wantRes: nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := &server{
				OrchestratorServer: tt.fields.OrchestratorService,
				storage:            tt.fields.storage,
			}
			gotRes, err := srv.StartMonitoring(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("StartMonitoring() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotRes, tt.wantRes) {
				t.Errorf("StartMonitoring() gotRes = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}

func TestNewServer(t *testing.T) {
	type args struct {
		opts []service.ServiceOption[server]
	}
	tests := []struct {
		name string
		args args
		want *server
	}{
		{
			name: "Set evaluation address",
			args: args{
				[]service.ServiceOption[server]{
					WithEvalManagerAddress("someAddress:9090"),
				},
			},
			want: &server{
				evalManagerAddress: "someAddress:9090",
				interval:           DefaultInterval,
			},
		},
		{
			name: "Set interval",
			args: args{
				[]service.ServiceOption[server]{
					WithInterval(10),
				},
			},
			want: &server{
				interval: 10,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewServer(tt.args.opts...)
			assert.Equal(t, tt.want.interval, got.interval, "Interval check")
			assert.Equal(t, tt.want.evalManagerAddress, got.evalManagerAddress, "EvalManager address check")
		})
	}
}
