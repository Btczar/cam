// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"

	"clouditor.io/clouditor/api/orchestrator"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
)

// RegisterCloudService is a wrapper around Clouditor orchestrator
func (s *server) RegisterCloudService(ctx context.Context, req *orchestrator.RegisterCloudServiceRequest) (*orchestrator.CloudService, error) {
	return s.OrchestratorServer.RegisterCloudService(ctx, req)
}

// UpdateCloudService is a wrapper around Clouditor orchestrator
func (s *server) UpdateCloudService(ctx context.Context, req *orchestrator.UpdateCloudServiceRequest) (*orchestrator.CloudService, error) {
	return s.OrchestratorServer.UpdateCloudService(ctx, req)
}

// GetCloudService is a wrapper around Clouditor orchestrator
func (s *server) GetCloudService(ctx context.Context, req *orchestrator.GetCloudServiceRequest) (*orchestrator.CloudService, error) {
	return s.OrchestratorServer.GetCloudService(ctx, req)
}

// ListCloudServices is a wrapper around Clouditor orchestrator
func (s *server) ListCloudServices(ctx context.Context, req *orchestrator.ListCloudServicesRequest) (*orchestrator.ListCloudServicesResponse, error) {
	return s.OrchestratorServer.ListCloudServices(ctx, req)
}

// RemoveCloudService is a wrapper around Clouditor orchestrator
func (s *server) RemoveCloudService(ctx context.Context, req *orchestrator.RemoveCloudServiceRequest) (*emptypb.Empty, error) {
	return s.OrchestratorServer.RemoveCloudService(ctx, req)
}

// ConfigureCloudService configures the cloud service with the corresponding service configuration
func (s *server) ConfigureCloudService(_ context.Context, req *configuration.ConfigureCloudServiceRequest) (res *configuration.ConfigureCloudServiceResponse, err error) {
	res = new(configuration.ConfigureCloudServiceResponse)
	// TODO(oxisto): Validate request

	// Check if an entry with the given serviceId is present, otherwise the following for loop crashes
	if _, ok := s.configurations[req.ServiceId]; !ok {
		s.configurations[req.ServiceId] = make(map[string]*collection.ServiceConfiguration)
	}

	// Just store it in the map for now
	for _, c := range req.Configurations.Configurations {
		if s.configurations[req.ServiceId][c.CollectionModule.String()] != nil {
			return
		}

		s.configurations[req.ServiceId][c.CollectionModule.String()] = c
	}
	return
}
