// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"
	"time"

	"clouditor.io/clouditor/api/orchestrator"
	"github.com/go-co-op/gocron"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
)

// StartMonitoring enables the monitoring of the specified control IDs for the given service.
func (srv *server) StartMonitoring(_ context.Context, req *configuration.StartMonitoringRequest) (
	res *configuration.StartMonitoringResponse, err error) {
	var (
		collectionModules []*collection.CollectionModule
		controls          []*orchestrator.Requirement
		metricIDs         []string
	)

	// Validate request
	if err = req.Validate(); err != nil {
		err = status.Error(codes.InvalidArgument, err.Error())
		return
	}

	// Retrieve all control objects for the IDs
	err = srv.storage.List(&controls, "id", true, 0, -1, "ID IN ?", req.ControlIds)
	if err != nil {
		err = status.Errorf(codes.Internal, "database error: %v", err)
		return
	}
	if len(controls) == 0 {
		err = status.Errorf(codes.Internal, "no controls")
		return
	}

	// Populate metric IDs
	for _, control := range controls {
		for _, m := range control.Metrics {
			metricIDs = append(metricIDs, m.Id)
		}
	}

	// List all collection modules that match our metric IDs
	err = srv.storage.List(&collectionModules, "id", true, 0, -1, "Metric_ID IN ?", metricIDs)
	if err != nil {
		err = status.Errorf(codes.Internal, "database error: %v", err)
		return
	}

	// Start collection modules and run them every $DefaultInterval minutes
	scheduler := gocron.NewScheduler(time.UTC)
	for _, cm := range collectionModules {
		_, err = scheduler.Every(DefaultInterval).Minutes().Do(func() {
			log.Infof("Start collection module `%s`", cm.Name)
			srv.startCollectionModule(cm, req.ServiceId)
		})
		if err != nil {
			log.Errorf("Could not start scheduler for `startCollectionModule` for %s: %v",
				cm.Name, err)
		}
	}
	// Start all collection module jobs
	scheduler.StartAsync()

	return
}

// func (srv *server) GetMonitoringStatus(_ context.Context, req *configuration.GetMonitoringStatusRequest) (res *configuration.MonitoringStatus, err error) {
// 	return nil, nil
// }
//
// func (srv *server) StopMonitoring(_ context.Context, req *configuration.StopMonitoringRequest) (res *configuration.StopMonitoringResponse, err error) {
// 	return nil, nil
// }
