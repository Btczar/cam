// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"

	"github.com/google/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
)

// AddCollectionModule adds a collection module to the server
func (srv *server) AddCollectionModule(_ context.Context, req *configuration.AddCollectionModuleRequest) (
	res *collection.CollectionModule, err error) {

	// Make sure, that we generate a new unique ID for the module
	req.Module.Id = uuid.NewString()

	// Just save it, I don't care
	err = srv.storage.Save(req.Module)
	if err != nil {
		err = status.Errorf(codes.Internal, "database error: %v", err)
		return
	}

	res = req.Module

	return
}

// startCollectionModule triggers the collection of the given collection module
func (srv *server) startCollectionModule(cm *collection.CollectionModule, serviceID string) {
	// Create connection to the collection module
	conn, err := grpc.Dial(cm.Address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Errorf("Could not dial to `%s`: %v", cm.Name, err)
		return
	}

	collectionClient := collection.NewCollectionClient(conn)
	_, err = collectionClient.StartCollecting(context.TODO(), &collection.StartCollectingRequest{
		ServiceId:     serviceID,
		MetricId:      cm.MetricId,
		EvalManager:   srv.evalManagerAddress,
		Configuration: srv.GetServiceConfiguration(serviceID, cm),
	})
	if err != nil {
		log.Errorf("Could not start collection module `%s`: %v", cm.Name, err)
	}
}

// func (srv *server) FindCollectionModule(_ context.Context, req *configuration.FindCollectionModuleRequest) (res *collection.CollectionModule, err error) {
// 	return nil, nil
// }
//
// func (srv *server) ListCollectionModules(_ context.Context, req *configuration.ListCollectionModulesRequest) (res *configuration.ListCollectionModulesResponse, err error) {
// 	return nil, nil
// }
//
// func (srv *server) RemoveCollectionModule(_ context.Context, req *configuration.RemoveCollectionModuleRequest) (res *emptypb.Empty, err error) {
// 	return nil, nil
// }

// GetServiceConfiguration return the service configuration for the corresponding serviceID. If there is no config,
// return empty config (no error)
func (srv *server) GetServiceConfiguration(serviceID string, cm *collection.CollectionModule) (serviceConfig *collection.ServiceConfiguration) {
	serviceConfig = new(collection.ServiceConfiguration)

	configurations, exists := srv.configurations[serviceID]
	// If not exist, return empty config.
	if !exists {
		return
	}

	// Get serviceConfig. If not exist, returns empty config.
	serviceConfig = configurations[cm.Name]
	return
}
