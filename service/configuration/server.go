// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"clouditor.io/clouditor/persistence/gorm"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"

	"clouditor.io/clouditor/api/orchestrator"
	"clouditor.io/clouditor/persistence"
	orchestratorservice "clouditor.io/clouditor/service/orchestrator"
	"github.com/sirupsen/logrus"
)

const (
	// DefaultInterval sets the default interval in minutes for collecting evidences
	DefaultInterval    = 5
	DefaultMetricsFile = "metrics.json"
)

var (
	log = logrus.WithField("service", "configuration")
)

type server struct {
	configuration.UnimplementedConfigurationServer

	// piggyback a Clouditor Orchestrator to take care of metrics and cloud services
	orchestrator.OrchestratorServer

	// evalManagerAddress defines the target address (evaluation) for the collection modules
	evalManagerAddress string
	// interval defines the interval in minutes for collecting evidences
	interval int

	// storage is our storage backend
	storage persistence.Storage

	// Map from CloudService (UUID) to map from CollectionModule (ID/NAME) to configurations
	// TODO(oxisto): move to storage backend
	configurations map[string]map[string]*collection.ServiceConfiguration
}

// WithEvalManagerAddress is a server option setting the address for the evaluation manager
func WithEvalManagerAddress(url string) service.ServiceOption[server] {
	return func(srv *server) {
		srv.evalManagerAddress = url
	}
}

// WithInterval is a server option setting the interval in minutes for collecting evidences
func WithInterval(interval int) service.ServiceOption[server] {
	return func(srv *server) {
		srv.interval = interval
	}
}

// WithStorage is an option to set the storage. If not set, NewServer will use inmemory storage.
func WithStorage(storage persistence.Storage) service.ServiceOption[server] {
	return func(srv *server) {
		srv.storage = storage
	}
}

// NewServer creates a new server that implements the configuration interface.
func NewServer(opts ...service.ServiceOption[server]) (svr *server) {
	var (
		controls []*orchestrator.Requirement
		err      error
	)
	svr = &server{
		interval:       DefaultInterval,
		configurations: map[string]map[string]*collection.ServiceConfiguration{},
	}

	// Apply any options
	for _, o := range opts {
		o(svr)
	}

	// Initialize in-memory storage backend if storage is not set already
	if svr.storage == nil {
		svr.storage, err = gorm.NewStorage(
			gorm.WithAdditionalAutoMigration(collection.CollectionModule{}),
			gorm.WithInMemory(),
			gorm.WithMaxOpenConns(1), // Otherwise race conditions might occur due to in-memory option (= sqlite)
		)
		if err != nil {
			log.Errorf("Could not initialize storage. Configuration server will probably not function correctly: %v", err)
		}
	}

	// Load controls (requirements in Clouditor terminology)
	log.Info("Loading controls")
	controls, err = loadRequirements()
	if err != nil {
		log.Errorf("Could not load controls (=requirements). Configuration server will probably not function"+
			" correctly: %v", err)
	}
	// Create a new embedded Clouditor orchestrator, which takes care of the heavy lifting of metrics and controls
	svr.OrchestratorServer = orchestratorservice.NewService(
		orchestratorservice.WithExternalMetrics(loadMetrics),
		orchestratorservice.WithRequirements(controls),
		orchestratorservice.WithStorage(svr.storage),
	)

	if svr.evalManagerAddress == "" {
		log.Error("Address for Eval Manager not set: CMs will probably not work properly (It can be set via " +
			"`WithEvalManagerAddress` option)")
	}

	return
}
