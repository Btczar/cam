// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"clouditor.io/clouditor/api/assessment"
	"clouditor.io/clouditor/api/orchestrator"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/oscal"
)

// ListMetrics is a wrapper around Clouditor orchestrator
func (s *server) ListMetrics(ctx context.Context, req *orchestrator.ListMetricsRequest) (res *orchestrator.ListMetricsResponse, err error) {
	return s.OrchestratorServer.ListMetrics(ctx, req)
}

// GetMetric is a wrapper around Clouditor orchestrator
func (s *server) GetMetric(ctx context.Context, req *orchestrator.GetMetricRequest) (res *assessment.Metric, err error) {
	return s.OrchestratorServer.GetMetric(ctx, req)
}

func metricFor(control *oscal.Control) (metrics []*assessment.Metric) {
	for _, prop := range control.Props {
		// Look for "metrics" property
		if prop.Name == "metrics" {
			// Remove whitespaces, e.g. "TlsVersion, TlsCipherSuite" -> "TlsVersion,TlsCipherSuite"
			var value = strings.ReplaceAll(prop.Value, " ", "")
			var metricIDs = strings.Split(value, ",")
			for _, metricID := range metricIDs {
				metrics = append(metrics, &assessment.Metric{
					Id: metricID,
				})
			}
		}
	}

	return
}

// loadMetrics loads metric definitions from a JSON file.
func loadMetrics() (metrics []*assessment.Metric, err error) {
	log.Infof("Loading metrics")
	var (
		b    []byte
		file = DefaultMetricsFile
	)

	b, err = os.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("error while loading %s: %w", file, err)
	}

	err = json.Unmarshal(b, &metrics)
	if err != nil {
		return nil, fmt.Errorf("error in JSON marshal: %w", err)
	}

	return
}
