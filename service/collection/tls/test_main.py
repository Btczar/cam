#!/usr/bin/env python3
import generate_protos
from main import *

class EvaluationMock(EvaluationServicer):
  def SendEvidences(self, request_iterator, context):
    info("starting evaluation mock")
    for e in request_iterator:
      esline=str(e).replace('\n',' ')
      info(f"Evidence arrived: {esline} {self.bindto}")
    return empty_pb2.Empty()

  max_workers = 10
  def __init__(self,bindto='127.0.0.1:50100'):
    self.bindto=bindto
    self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=self.max_workers))
    add_EvaluationServicer_to_server(self, self.server)
    self.server.add_insecure_port(self.bindto)
    self.server.start()

  def stop(self,grace=None):
    return self.server.stop(grace)


if __name__ == '__main__':
    setuplogger(logging.DEBUG)
    evaluationmock  = EvaluationMock()
    evaluationmock2 = EvaluationMock('127.0.0.2:50100')

    #evalmanagers={}
    #evalmanagers['localhost:50100'] = EvidenceUploader('localhost:50100')
    #evalmanagers['127.0.0.2:50100'] = EvidenceUploader('127.0.0.2:50100')
    #debug("SENDING EVIDENCES")
    #evalmanagers['localhost:50100'].evidences.put(Evidence(id='666',target_service='www.trololo.com', name=f"results"))
    #evalmanagers['127.0.0.2:50100'].evidences.put(Evidence(id='667',target_service='www.troo.com', name=f"resulteeees"))
    #evalmanagers['localhost:50100'].evidences.put(Evidence())
    #evalmanagers['127.0.0.2:50100'].evidences.put(Evidence())
    #sleep(10)
    #exit()
    collector = CollectorTls()

    testrequests = [StartCollectingRequest(service_id='www.aisec.fraunhofer.de:443',metric_id='temperature',eval_manager='127.0.0.1:50100')] 
    testrequests+= [StartCollectingRequest(service_id='www.google.com:443',metric_id='temperature',eval_manager='127.0.0.2:50100')]
    collector.client.StartCollecting(testrequests[0])
    collector.client.StartCollecting(testrequests[1])
    collector.client.StartCollectingStream(iter(testrequests))
    sleep(14)
    collector.stop(grace=3)
    evaluationmock.stop(grace=3)
    evaluationmock2.stop(grace=3)
    sleep(4)

    
   
