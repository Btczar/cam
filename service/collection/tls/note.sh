#proto generation from commandline
python -m grpc_tools.protoc --proto_path=../../../ --proto_path=../../../third_party/ --python_out=. --grpc_python_out=.  api/common/evidence.proto api/common/metric.proto  api/evaluation/evaluation.proto api/collection/collection.proto

#tests
pytest --doctest-modules --ignore=api --ignore=google

#doc generation
pip install sphinx sphinx-markdown-builder
sphinx-apidoc -o Sphinx-docs . sphinx-apidoc --full -A 'Matteo Ferla'; cd Sphinx-docs
make markdown
#https://stackoverflow.com/questions/36237477/python-docstrings-to-github-readme-md



sudo docker run -it --rm ubuntu:14.04 bash -c 'echo GET / | openssl s_client -quiet -connect www.sz.de:443 -tls1_2'

openssl s_client -ssl3 -connect www.python.org
openssl s_client -connect "downloads.dell.com:443"  </dev/null
sudo docker run -it --rm ubuntu:14.04 bash -c 'openssl s_client -connect "downloads.dell.com:443" -ssl3  </dev/null'
sudo docker run -it --rm ubuntu:14.04 openssl ciphers



####TTTT, the openssl ciphers doesnt show all the ciphers. the tls<1.3 needs -cipher and not -ciphersuite!
#140500748195072:error:1426E0B9:SSL routines:ciphersuite_cb:no cipher match:ssl/ssl_ciph.c:1294:
#above means the openssl failed before making the connection
#CONNECTED means that openssl tried to send ClientHello
#usually if server rejects cipherlist you get:  140572116006560:error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure:s23_clnt.c:770:
#https://www.openssl.org/docs/manmaster/man1/openssl-ciphers.html#CIPHER-LIST-FORMAT
sudo docker run -it --rm ubuntu:14.04 openssl s_client -connect www.cloudflare.com:443 -cipher 'ADH'  < /dev/null
sudo docker run -it --rm ubuntu:14.04 openssl s_client -connect www.cloudflare.com:443 -cipher 'RC4'  < /dev/null
sudo docker run -it --rm ubuntu:14.04 openssl s_client -connect www.cloudflare.com:443 -cipher 'LOW'  < /dev/null
sudo docker run -it --rm ubuntu:14.04 openssl s_client -connect www.cloudflare.com:443 -cipher 'NULL' < /dev/null
sudo docker run -it --rm ubuntu:14.04 openssl s_client -connect www.cloudflare.com:443 -cipher '3DES' < /dev/null
