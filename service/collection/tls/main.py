#!/usr/bin/env python3
# Copyright (c) 2022 Fraunhofer AISEC
# Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This is the TLS collector module.

It implements the Collection grpc service, and delivers results to Evaluation grpc service.

"""

from concurrent.futures import thread
from time import sleep
import os,random,logging
from logging import debug,info,warn,error
from typing import Iterable
SDIR=os.path.dirname(os.path.abspath(__file__))

import grpc,sys,socket,ssl
import generate_protos

from google.protobuf import empty_pb2
from google.protobuf import timestamp_pb2
#from google.api import annotations_pb2

from api.common.evidence_pb2 import *
from api.common.metric_pb2 import *
from api.collection.collection_pb2 import *       #messages
from api.collection.collection_pb2_grpc import *  #services
from api.evaluation.evaluation_pb2 import *
from api.evaluation.evaluation_pb2_grpc import *


from concurrent import futures
import threading
from queue import Queue                  #from multiprocessing import Queue
executor = futures.ThreadPoolExecutor()  #executor = futures.ProcessPoolExecutor(max_workers=10)

class EvidenceUploader(threading.Thread):
  evidences = Queue()
  def __init__(self,eval_manager):
    super().__init__()
    self.eval_manager = eval_manager
    self.start()
  def run(self):
    debug(f"starting evidence uploader for eval manager {self.eval_manager}, {self}")
    EvaluationStub(grpc.insecure_channel(self.eval_manager)).SendEvidences(iter(self.evidences.get,Evidence())) #iter() is wrapping the queue to be iterable

evalmanagers = dict[str,EvidenceUploader]()

class CollectorTls(CollectionServicer):
  monkeys = dict[int,futures.Future]()
  def __init__(self, bindto='0.0.0.0:50051', max_workers=100):        #'[::]:50051'):
    info(f"TLS collector starting at {bindto}, {ssl.OPENSSL_VERSION}")
    self.bindto=bindto
    self.max_workers=max_workers
    self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=self.max_workers))
    add_CollectionServicer_to_server(self, self.server)
    self.server.add_insecure_port(self.bindto)
    self.server.start()

  def StartCollecting(self, req, context):
    info('StartCollectingRequest:' + str(req).replace('\n',' '))
    jobid = random.randint(1,1000000) #TODO: there is a type conflict between Evidence and StartCollectingResponse
    if not req.eval_manager in evalmanagers:
      debug(f"creating EvidenceUploader for eval_manager {req.eval_manager}")
      evalmanagers[req.eval_manager] = EvidenceUploader(req.eval_manager)
    self.monkeys[jobid] = executor.submit(self.measure, jobid, req.service_id, req.metric_id, req.eval_manager)
    return StartCollectingResponse(id=str(jobid))

  def StopCollecting(self, request, context):
    if not self.monkeys[request.id].cancel(): warn(f"Job {request.id} is already running or completed")
    return empty_pb2.Empty()

  def StartCollectingStream(self, request_iterator, context):
    for screq in request_iterator:
      self.StartCollecting(screq, context=None)
    return empty_pb2.Empty()

  def stop(self,grace=None):
    executor.shutdown(cancel_futures=True)   #for jobid,futu in self.monkeys.items(): futu.cancel()
    #StopIteration of Evidence queues, terminate evidenceuplader threads
    for evalmanager,v in evalmanagers.items():
      debug(f"stopping {v}")
      v.evidences.put(Evidence()) #empty evidence means end of the queue
      v.join()
    return self.server.stop(grace)
  
  @property
  def client(self):
    return CollectionStub(grpc.insecure_channel(self.bindto))

  def measure(self, jobid, service_id, metric_id, eval_manager:str):
    hostname,port=service_id.split(':')
    debug(f"Starting measuring {hostname}:{port}")
    cipher,version, keylength = tlsmeasure(hostname,port)
    #e = Evidence(id=jobid,target_service=service_id,target_resource='gigo',gathered_using=metric_id) #TODO:gathered_at,value,raw_evidence
    debug(f"Sending evidence from job {jobid} to {eval_manager}. Evidence:{cipher} {version} {keylength}")
    evalmanagers[eval_manager].evidences.put(Evidence(id=str(jobid),target_service=service_id, name=f"{cipher} {version} {keylength}"))

#TODO: move to separate file
def tlsmeasure(hostname,port=443):
    try:
      context = ssl.create_default_context()
      #con2 = ssl.SSLContext(ssl.PROTOCOL_TLSv1_1)
      with socket.create_connection((hostname, port)) as sock:
        with context.wrap_socket(sock, server_hostname=hostname) as ssock:
          return  ssock.cipher()
    except (ssl.SSLError, ssl.SSLEOFError) as e:
      warn(f"Fail {e}. {hostname}:{port}")
    except Exception as e:
      warn(f"FAIL generic {e}. {hostname}:{port}")

from subprocess import call,DEVNULL
def tlsmeasure_withopenssl(hostname,port=443):
  supported_protocols = []
  for proto in [ 'tls1', 'tls1_1', 'tls1_2', 'tls1_3']:
    exitcode = call(f"openssl s_client -connect {hostname}:{port} -{proto} < /dev/null", shell=True, stderr=DEVNULL, stdout=DEVNULL)
    if not exitcode: supported_protocols.append(proto)
  info(f"{hostname}:{port}: {supported_protocols}")


def setuplogger(level=logging.DEBUG):
  logging.basicConfig(stream=sys.stdout, style='{', format='{asctime}: {levelname:>7s} {threadName:24s} {module:>9s}:{lineno:>4d} {funcName:22s} {message}', level=level)

if __name__ == '__main__':
    setuplogger(logging.INFO)
    collector = CollectorTls()
    collector.server.wait_for_termination()

   
