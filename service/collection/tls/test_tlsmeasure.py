#!/usr/bin/env python3
from main import *

setuplogger(logging.INFO)
hosts = """
www.google.com
www.amazon.de
www.sz.de
www.heise.de
www.berlin.de
www.youtube.com
www.fraunhofer.de
www.aisec.fraunhofer.de
www.gitlab.com
www.python.org
muenchen.de
fcbayern.com
www.swm.de
www.bmw.de
www.data-infrastructure.eu
gaia-x.eu
www.finanzamt.bayern.de
www.tum.de
www.elster.de
""".split()

#for host in hosts: debug(f"{host} {tlsmeasure(host)}")
for host in hosts: debug(f"{host} {tlsmeasure_withopenssl(host)}")

