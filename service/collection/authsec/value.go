package authsec

import "clouditor.io/clouditor/voc"

type Value struct {
	voc.Resource

	OAuthGrantTypes `json:"oAuth"`
}

type OAuthGrantTypes struct {
	GrantTypes []string `json:"grantTypes"`
}
