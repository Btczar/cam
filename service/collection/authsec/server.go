// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package authsec contains service specific code for the Authentication Security Collection Module
package authsec

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"clouditor.io/clouditor/voc"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/strct"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"

	api_clouditor "clouditor.io/clouditor/api"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	//"golang.org/x/tools/go/analysis/passes/nilfunc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	log = logrus.WithField("service", "collection-authsec")
)

type server struct {
	collection.UnimplementedCollectionServer
	streams  *api_clouditor.StreamsOf[evaluation.Evaluation_SendEvidencesClient, *common.Evidence]
	grpcOpts []grpc.DialOption
}

// WithAdditionalGRPCOpts is an option to configure additional gRPC options.
func WithAdditionalGRPCOpts(opts ...grpc.DialOption) service.ServiceOption[server] {
	return func(s *server) {
		s.grpcOpts = opts
	}
}

func NewServer(opts ...service.ServiceOption[server]) collection.CollectionServer {
	s := &server{
		streams: api_clouditor.NewStreamsOf(api_clouditor.WithLogger[evaluation.Evaluation_SendEvidencesClient, *common.Evidence](log)),
	}

	// Apply any options
	for _, o := range opts {
		o(s)
	}

	return s
}

func getAndValidateMetadata(config *collection.ServiceConfiguration_AuthenticationSecurityConfig) (*map[string]interface{}, *common.Error) {
	// Parse issuer identifier
	issuerID := config.AuthenticationSecurityConfig.Issuer
	if issuerID == "" {
		return nil, &common.Error{
			Code:        common.Error_ERROR_INVALID_CONFIGURATION,
			Description: "No issuer identifier given",
		}
	}
	issuer, err := url.Parse(issuerID)
	if err != nil {
		return nil, &common.Error{
			Code:        common.Error_ERROR_INVALID_CONFIGURATION,
			Description: "Error parsing issuer identifier: " + err.Error(),
		}
	}

	// Optionally parse metadataURL if given explicitly
	var metadataURL *url.URL = nil
	if metadataDocument := config.AuthenticationSecurityConfig.MetadataDocument; metadataDocument != "" {
		metadataURL, err = url.Parse(metadataDocument)
		if err != nil {
			return nil, &common.Error{
				Code:        common.Error_ERROR_INVALID_CONFIGURATION,
				Description: "Error parsing metadata URL: " + err.Error(),
			}
		}
	}

	// Fetch the metadata document
	metadata, errStruct := getMetadata(issuer, metadataURL)
	if errStruct != nil {
		return nil, errStruct
	}
	logrus.Traceln("Found Metadata document for " + (*metadata)["issuer"].(string))

	// Sanity check it
	metadata, err = checkMetadataRFC8414(metadata)
	if err != nil {
		if err != nil {
			return nil, &common.Error{
				Code:        common.Error_ERROR_PROTOCOL_VIOLATION,
				Description: "Error validating Metadata document (RFC 8414): " + err.Error(),
			}
		}
	}
	metadata, err = checkMetadataOIDCDiscovery(metadata)
	if err != nil {
		return nil, &common.Error{
			Code:        common.Error_ERROR_PROTOCOL_VIOLATION,
			Description: "Error validating Metadata document (OIDC Discovery): " + err.Error(),
		}
	}

	return metadata, nil
}

// checkConfiguration performs some basic input validation on the configuration data sent with the request
// Note that this is merely a static best effort and errors may arise later on.
// If that is the case, the errors are reported to the evaluation manager instead of the caller.
func checkConfiguration(req *collection.StartCollectingRequest) (config *collection.ServiceConfiguration_AuthenticationSecurityConfig, err error) {
	config = req.Configuration.RawConfiguration.(*collection.ServiceConfiguration_AuthenticationSecurityConfig)
	// Different metric IDs may need different parameters
	// We only check the ones relevant to the current metric id
	switch req.MetricId {
	case "OAuthGrantTypes":
		// Required Config: issuer (URL)
		if config.AuthenticationSecurityConfig.Issuer == "" {
			err = errors.New("missing required config parameter: issuer")
			return
		}
		if _, err = url.Parse(config.AuthenticationSecurityConfig.Issuer); err != nil {
			err = errors.New("error parsing issuer: " + err.Error())
			return
		}
		// Optional Config: metadata_document (URL)
		if metadata := config.AuthenticationSecurityConfig.MetadataDocument; metadata != "" {
			if _, err = url.Parse(metadata); err != nil {
				err = errors.New("error parsing metadata_document: " + err.Error())
				return
			}
		}
		break
	default:
		err = errors.New("unknown Metric ID")
		return
	}
	return
}

// handleCollectionRequest runs the actual Collection Request, after the initial sanity checks.
// If any problems arise, an error is reported to the evaluation manager
func handleCollectionRequest(requestId string, serviceId string, metricId string,
	evidenceStream *api_clouditor.StreamChannelOf[evaluation.Evaluation_SendEvidencesClient, *common.Evidence],
	config *collection.ServiceConfiguration_AuthenticationSecurityConfig) {

	// Prepare evidence struct
	evidence := &common.Evidence{
		Id:            requestId,
		Name:          requestId,
		TargetService: serviceId,
		GatheredUsing: metricId,
		GatheredAt:    timestamppb.Now(),
		ToolId:        collection.ServiceConfiguration_AUTHENTICATION_SECURITY.String(),
	}

	// Gather relevant evidence for the metric
	switch metricId {
	case "OAuthGrantTypes":
		metadata, errStruct := getAndValidateMetadata(config)
		if nil != errStruct {
			evidence.Error = errStruct
			break
		}
		rawEvidence, err := json.Marshal(metadata)
		if nil != err {
			evidence.Error = &common.Error{
				Code:        common.Error_ERROR_UNKNOWN, // TODO: Should be "Internal error"
				Description: "Error encoding raw evidence: " + err.Error(),
			}
			break
		}
		evidence.RawEvidence = string(rawEvidence)
		grantTypes, err := shouldBeStringArrayOrNil(metadata, "grant_types_supported")
		if nil != err {
			evidence.Error = &common.Error{
				Code:        common.Error_ERROR_UNKNOWN, // TODO: Should be "Internal error"
				Description: "Error extracting evidence: " + err.Error(),
			}
			break
		}
		evidenceValue := Value{
			Resource: voc.Resource{
				// ID and Type has to be set. Otherwise, evaluation will fail due to evidence validation
				ID:   "JustNotEmpty",
				Type: []string{"SomeType"},
			},
			OAuthGrantTypes: OAuthGrantTypes{GrantTypes: grantTypes},
		}
		evidence.Value, err = strct.ToValue(evidenceValue)
		if err != nil {
			err = fmt.Errorf("could not convert struct to structpb.Value: %w", err)
			log.Error(err)
			break
		}
		break
	default:
		evidence.Error = &common.Error{
			Code:        common.Error_ERROR_INVALID_CONFIGURATION,
			Description: "Invalid Metric ID: " + metricId,
		}
	}

	if nil != evidence.Error {
		log.Warnln("Reporting an error: " + evidence.Error.Description)
	} else {
		log.Tracef("Sending '%v' to Evaluation Manager", evidence.Value)
	}

	evidenceStream.Send(evidence)
	log.Infof("Sent evidence '%s'", evidence.Id)
}

func (s *server) StartCollecting(_ context.Context, req *collection.StartCollectingRequest) (*collection.StartCollectingResponse, error) {
	log.Infoln("Received request")

	// Parse and check configuration data
	// If problems are detected at this stage, they are returned to the caller
	// instead of being forwarded to the evaluation manager
	config, err := checkConfiguration(req)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "%v", err)
	}

	// Get stream for the Evaluation Manager
	component := "Evaluation Manager"
	evidenceStream, err := s.streams.GetStream(req.EvalManager, component, api.InitEvalStream, s.grpcOpts...)
	if err != nil {
		return nil, status.Errorf(codes.FailedPrecondition, "Could not connect to Eval Manager: %v", err)
	}

	// Generate new Request ID
	requestId := uuid.NewString()
	log.Traceln("Metric Id: " + req.MetricId)

	// Handle the actual request in a separate goroutine
	// StartCollecting will return and later collection problems are reported to the evaluation manager

	go handleCollectionRequest(requestId, req.ServiceId, req.MetricId, evidenceStream, config)
	return &collection.StartCollectingResponse{Id: requestId}, nil
}

func (s *server) StopCollecting(_ context.Context, _ *collection.StopCollectingRequest) (*emptypb.Empty, error) {
	return nil, status.Error(codes.Unimplemented, "StopCollection not implemented")
}
