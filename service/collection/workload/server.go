// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package workload contains service specific code for the Workload Configuration Collection Module.
package workload

import (
	"context"
	"fmt"
	"sync"

	clapi "clouditor.io/clouditor/api"
	clapidiscovery "clouditor.io/clouditor/api/discovery"
	"clouditor.io/clouditor/service/discovery/k8s"
	"clouditor.io/clouditor/voc"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/structpb"
	"k8s.io/client-go/kubernetes"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection/workload"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/strct"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"
	. "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection/workload/openstack"
	openstackstrct "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection/workload/openstack/strct"
)

var (
	log = logrus.WithField("service", "collection-workload")
)

// server is an implementation of the Workload Configuration service.
type server struct {
	collection.UnimplementedCollectionServer

	// Stream to Evaluation Manager component
	stream *clapi.StreamsOf[evaluation.Evaluation_SendEvidencesClient, *common.Evidence]

	grpcOpts []grpc.DialOption

	// Provider Configuration with the Cloud serviceID as key
	providerConfigs map[string]providerConfiguration
	// Mutex for provider configs
	providerConfigsMutex sync.Mutex
}

// providerConfiguration contains the configs for
// * Kubernetes
// * Openstack
type providerConfiguration struct {
	kubernetes *kubernetes.Clientset
	openstack  *openstack.AuthOptions
}

// WithAdditionalGRPCOpts is an option to configure additional gRPC options.
func WithAdditionalGRPCOpts(opts ...grpc.DialOption) service.ServiceOption[server] {
	return func(s *server) {
		s.grpcOpts = opts
	}
}

// NewServer creates a new workload server with default values.
func NewServer(opts ...service.ServiceOption[server]) collection.CollectionServer {
	s := &server{
		stream:          clapi.NewStreamsOf(clapi.WithLogger[evaluation.Evaluation_SendEvidencesClient, *common.Evidence](log)),
		grpcOpts:        []grpc.DialOption{},
		providerConfigs: make(map[string]providerConfiguration),
	}

	// Apply any options
	for _, o := range opts {
		o(s)
	}

	return s
}

// StartCollecting starts collecting configurations from Kubernetes and OpenStack, creates evidences and sends the
// evidences to the Evaluation Manager.
func (srv *server) StartCollecting(_ context.Context, req *collection.StartCollectingRequest) (
	resp *collection.StartCollectingResponse, err error) {
	log.Tracef("Received StartCollecting Request for Service ID '%v'", req.ServiceId)

	// Validate StartCollectingRequest
	if err = req.Validate(); err != nil {
		err = status.Error(codes.InvalidArgument, err.Error())
		log.Debug(err)
		return
	}

	// Extract protobuf rawRonfiguration to Config type
	conf, ok := req.Configuration.GetRawConfiguration().(*collection.ServiceConfiguration_WorkloadSecurityConfig)
	if !ok {
		err = status.Errorf(codes.InvalidArgument, collection.ErrInvalidWorkloadConfigurationRawConfiguration.Error())
		log.Debug(err)
		return
	}

	// Validate the StartCollectingRequest.RawConfiguration
	// TODO (anatheka): after merging MR #74 (...Validate Method) we must add the CollectionModuleValidator call here
	// err := collection.Validate[Config](req, validator)
	// Convert and store provider configuration
	err = srv.addProviderConfig(req, conf)
	if err != nil {
		err = fmt.Errorf("could not add provider configuration: %w", err)
		log.Error(err)
		return nil, status.Errorf(codes.Internal, "%v", err)
	}

	// Get stream for the Evaluation Manager
	stream, err := srv.stream.GetStream(req.EvalManager, TargetComponent, api.InitEvalStream, srv.grpcOpts...)
	if err != nil {
		err = fmt.Errorf("could not get stream to Evaluation Manager: %w", err)
		log.Debug(err)
		return nil, status.Errorf(codes.Internal, "%v", err)
	}

	// Get workload configurations
	results, err := srv.getWorkloadConfigurations(req)
	if err != nil {
		err = fmt.Errorf("could not retrieve configurations: %w", err)
		log.Error(err)
		return nil, status.Errorf(codes.Internal, "%v", err)
	}

	// Create CAM evidence and send to stream channel
	err = EnqueueEvidences("WorkloadConfiguration", req, results, stream, log)
	if err != nil {
		err = fmt.Errorf("could not enqueue CAM evidence in stream channel: %v", err)
		log.Error(err)
		return nil, status.Errorf(codes.Internal, "%v", err)
	}

	// Generate a new ID
	resp = &collection.StartCollectingResponse{
		Id: uuid.NewString(),
	}

	return
}

// StopCollecting stops collecting configurations
func (_ *server) StopCollecting(_ context.Context, _ *collection.StopCollectingRequest) (*emptypb.Empty, error) {
	// For now we do not implement the StopCollection
	return nil, status.Error(codes.Unimplemented, "StopCollection not implemented")
}

// getWorkloadConfigurations configures discoverers based on the given ServiceID and retrieves resources
func (srv *server) getWorkloadConfigurations(req *collection.StartCollectingRequest) ([]voc.IsCloudResource, error) {
	var (
		discoverer []clapidiscovery.Discoverer
		results    []voc.IsCloudResource
		err        error
	)

	// Set discoverer for existing provider configurations
	discoverer = srv.setDiscoverer(req.ServiceId)
	if discoverer == nil {
		err = fmt.Errorf("no discoverer available")
		log.Error(err)
		return nil, err
	}

	// Retrieve resources
	for _, v := range discoverer {
		list, err := v.List()
		if err != nil {
			err = fmt.Errorf("could not retrieve resources from %s: %v", v.Name(), err)
			log.Error(err)
		}
		results = append(results, list...)
	}

	return results, nil
}

// addProviderConfig stores the given provider configuration
func (srv *server) addProviderConfig(req *collection.StartCollectingRequest, conf *collection.ServiceConfiguration_WorkloadSecurityConfig) (err error) {
	// If serviceID is already available, return
	if _, ok := srv.providerConfigs[req.ServiceId]; ok {
		return
	}

	// TODO(lebogg): Should be done before already, I guess (`Validate`)?
	if conf == nil || conf.WorkloadSecurityConfig == nil {
		err = collection.ErrMissingServiceConfiguration
		return
	}

	// Set ServiceConfiguration for Kubernetes
	if k := conf.WorkloadSecurityConfig.Kubernetes; k != nil {
		err = srv.kubeConfig(k, req.ServiceId)
		if err != nil {
			return fmt.Errorf("%s: %w", collection.ErrInvalidKubernetesServiceConfiguration, err)
		}
		return nil
	}

	// Set ServiceConfiguration for OpenStack
	if os := conf.WorkloadSecurityConfig.Openstack; os != nil {
		err := srv.openstackConfig(os, req.ServiceId)
		if err != nil {
			return fmt.Errorf("%s: %w", collection.ErrInvalidOpenstackServiceConfiguration, err)
		}

		return nil
	}

	return collection.ErrMissingServiceConfiguration

}

// kubeConfig stores the Kubernetes configuration
func (srv *server) kubeConfig(value *structpb.Value, serviceId string) error {
	// Get byte array from protobuf value
	v, err := strct.ToByteArray(value)
	if err != nil {
		return collection.ErrConversionProtobufToByteArray
	}

	// Get Kubernetes clientset
	clientset, err := workload.AuthFromKubeConfig(v)
	if err != nil {
		return fmt.Errorf("%s: %w", collection.ErrKubernetesClientset, err)
	}

	// Store Kubernetes clientset for the specific serviceID
	configValue := providerConfiguration{
		kubernetes: clientset,
	}

	srv.providerConfigsMutex.Lock()
	srv.providerConfigs[serviceId] = configValue
	srv.providerConfigsMutex.Unlock()

	return nil
}

// openstackConfig stores the OpenStack configuration
func (srv *server) openstackConfig(value *structpb.Value, serviceId string) error {

	// Get AuthOpts from protobuf value
	authOpts, err := openstackstrct.ToAuthOptions(value)
	if err != nil {
		return collection.ErrConversionProtobufToAuthOptions
	}

	// Store Openstack AuthOpts to the  specific serviceID
	configValue := providerConfiguration{
		openstack: authOpts,
	}
	srv.providerConfigsMutex.Lock()
	srv.providerConfigs[serviceId] = configValue
	srv.providerConfigsMutex.Unlock()

	return nil

}

// setDiscoverer sets discoverer for serviceID
func (srv *server) setDiscoverer(serviceID string) (discoverer []clapidiscovery.Discoverer) {
	// Add Kubernetes discoverer for compute and storage
	if srv.providerConfigs[serviceID].kubernetes != nil {
		discoverer = append(discoverer, k8s.NewKubernetesComputeDiscovery(srv.providerConfigs[serviceID].kubernetes), k8s.NewKubernetesNetworkDiscovery(srv.providerConfigs[serviceID].kubernetes))

		return
	}

	// Add Openstack discoverer for compute and storage
	if srv.providerConfigs[serviceID].openstack != nil {
		discoverer = append(discoverer, openstack.NewStorageDiscovery(openstack.WithAuthOpts(srv.providerConfigs[serviceID].openstack)), openstack.NewComputeDiscovery(openstack.WithAuthOpts(srv.providerConfigs[serviceID].openstack)))

		return
	}

	return
}

func validator(req *collection.StartCollectingRequest) (conf *collection.ServiceConfiguration_WorkloadSecurityConfig, err error) {
	if req.Configuration.CollectionModule != collection.ServiceConfiguration_WORKLOAD_CONFIGURATION {
		err = collection.ErrInvalidCollectionModule
		return
	}

	// TODO(anatheka): Use addProvider logic here?
	//conf, err = strct.ToStruct[collection.WorkloadConfig](req.Configuration.GetRawConfiguration())
	//if err != nil {
	//	err = fmt.Errorf("invalid configuration: raw configuration is not of type 'Config' ('kubernetes' or 'openstack'): %s", err)
	//	return
	//}

	return
}
