// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package integrity

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"

	clapi "clouditor.io/clouditor/api"
	"clouditor.io/clouditor/voc"
	"github.com/Fraunhofer-AISEC/cmc/attestationreport"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api"
	apicollection "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/common"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection"
)

var (
	log      = logrus.WithField("service", "collection-integrity")
	metricId = "SystemComponentsIntegrity"
)

type server struct {
	apicollection.UnimplementedCollectionServer

	streams  *clapi.StreamsOf[evaluation.Evaluation_SendEvidencesClient, *common.Evidence]
	grpcOpts []grpc.DialOption
}

// WithAdditionalGRPCOpts is an option to configure additional gRPC options.
func WithAdditionalGRPCOpts(opts ...grpc.DialOption) service.ServiceOption[server] {
	return func(s *server) {
		s.grpcOpts = opts
	}
}

func NewServer(opts ...service.ServiceOption[server]) apicollection.CollectionServer {
	s := &server{
		streams: clapi.NewStreamsOf(clapi.WithLogger[evaluation.Evaluation_SendEvidencesClient, *common.Evidence](log)),
	}

	// Apply any options
	for _, o := range opts {
		o(s)
	}

	return s
}

func (s *server) StartCollecting(_ context.Context, in *apicollection.StartCollectingRequest) (
	res *apicollection.StartCollectingResponse, err error) {
	log.Tracef("Received StartCollecting Request for Service ID %v", in.ServiceId)

	if in.MetricId != metricId {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid Metric ID %v. Expected %v", in.MetricId,
			metricId)
	}

	rawConfig, ok := in.Configuration.GetRawConfiguration().(*apicollection.ServiceConfiguration_RemoteIntegrityConfig)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, apicollection.ErrInvalidRemoteIntegrityRawConfiguration.Error())
	}

	capem := []byte(rawConfig.RemoteIntegrityConfig.Certificate)

	// Collecting integrity information from external service requires nonce
	// to avoid replay attacks
	nonce := make([]byte, 8)
	_, err = rand.Read(nonce)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to generate random bytes (%v)", err)
	}

	log.Tracef("Collecting integrity information from service: %v\n", in.ServiceId)
	ar, err := Collect(rawConfig.RemoteIntegrityConfig.Target, nonce)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to collect information from service %v: %v",
			in.ServiceId, err)
	}
	log.Tracef("Collected integrity information from service: %v\n", in.ServiceId)

	result := attestationreport.Verify(string(ar), nonce, capem, nil)
	if !result.Success {
		log.Tracef("Verification of integrity information failed - Service is not trustworthy")
	}

	rawEvidence, err := json.Marshal(result)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to marshal integrity result: %v", err)
	}

	value := Value{
		Resource: voc.Resource{
			// ID and Type has to be set. Otherwise, evaluation will fail due to evidence validation
			ID:   "JustNotEmpty",
			Type: []string{"SomeType"},
		},
		SystemComponentsIntegrity: SystemComponentsIntegrity{Status: result.Success},
	}
	evidenceValue, err := toStruct(value)
	if err != nil {
		err = fmt.Errorf("could not convert struct to structpb.Value: %w", err)
		log.Error(err)
	}

	log.Tracef("Sending evidences to eval manager %v", in.EvalManager)

	// Get stream for the Evaluation Manager
	component := "Evaluation Manager"
	stream, err := s.streams.GetStream(in.EvalManager, component, api.InitEvalStream, s.grpcOpts...)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "%v", err)
	}

	requestId := uuid.NewString()

	evidence := &common.Evidence{
		Id:             requestId,
		Name:           requestId,
		TargetService:  in.ServiceId,
		TargetResource: result.PlainAttReport.DeviceDescription.Fqdn,
		GatheredUsing:  in.MetricId,
		ToolId:         collection.RemoteIntegrityComponent,
		GatheredAt:     timestamppb.Now(),
		Value:          evidenceValue,
		RawEvidence:    string(rawEvidence),
	}

	// Send evidence to stream via channel
	stream.Send(evidence)

	res = &apicollection.StartCollectingResponse{
		Id: requestId,
	}
	return
}

func (s *server) StopCollecting(_ context.Context, in *apicollection.StopCollectingRequest) (*emptypb.Empty, error) {
	log.Tracef("Received StopCollecting Request for Service ID %v", in.Id)

	return nil, status.Error(codes.Unimplemented, "StopCollection not implemented")
}
