# Continuous Automated Monitoring

This project contains the reference implementation of the Gaia-X Continuous Automated Monitoring component.

# Installation
Assuming that you have access to an kubernetes cluster, are in the right kubernetes context, and have helm installed:
```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/cam.git
helm upgrade --install your_name_here cam/helm
```


## Clarifications of the Specification

This implementation follows closely the specification of the [Continuous Automated Monitoring](https://www.gxfs.eu/download/1731/) component of the Gaia-X Federated Services. However, there are minor clarifications to the specification that we chose because the specification was ambiguous or contradictory.

### General

* Usage of `String` instead of `Int` for all ID fields: All ID fields are now UUIDs and their internal protobuf representation is a string. This was necessary for two reasons: First, some ID fields need to be compatible with OSCAL (such as the control ID) and their identifiers follow a string-based approach. Second, some identifiers, such as as the evidence ID need to be uniquely generated on their own by the collection modules, which makes it ideally suited for a UUID. This change is purely internal, as the change of representation to the outside world via the REST API is transparent.
* The specification did not include a path layout for the REST API. A sensible, future-compatible approach using a numbered schema, such as `/v1/configuration` has been chosen.

### Interface Additions

* The specification was missing function calls to add/edit/remove target cloud services. Appropriate RPC calls, such as `RegisterCloudService` have been added to the `Configuration` service interface and exposed via the REST API.
* The `Configuration` interface also lacked a possibility to configure the needed `ServiceConfiguration` for a particular cloud service. This functionality has been added via the `ConfigureCloudService` function call.
* A `ListControls` call has been added to the `Configuration` interface, that returns a list of all relevant controls configured in the CAM.
* Function calls to list and retrieve a particular evidence from the evaluation manager have been added to the `Evaluation` service interface in the form of `ListEvidence` and `GetEvidence` and exposed via the REST API.
* Usage of `oneof` fields in protobuf: The generic `google.protobuf.Value` typed raw configuration has been replaced by collection module-specific raw configurations (see _collection.proto_, e.g. the `CommunicationSecurityConfig` message). It improves the read- and usability of the API, in particular for the Dashboard since it now knows how to fill the raw configuration in each case. The collection modules use the generated configs instead of creating on their own ones.

### Object Additions

* An `Error` object class has been introduced and added as a property `error` to the `Evidence` class. The reasoning behind this, is that during the evidence collection an error might occur that leads to the generation of a corrupt / invalid evidence. Instead of silently ignoring errors, these can now be sent to the evaluation manager for further processing.
