// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package collection

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_StartCollectingRequest_validate(t *testing.T) {
	type args struct {
		req *StartCollectingRequest
	}
	tests := []struct {
		name     string
		args     args
		wantErr  bool
		wantResp assert.ValueAssertionFunc
	}{
		{
			name: "empty service id",
			args: args{
				req: &StartCollectingRequest{
					ServiceId: "",
				},
			},
			wantResp: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				err, _ := i1.(error)

				return assert.ErrorContains(t, err, ErrMissingServiceID.Error())
			},
			wantErr: true,
		},
		{
			name: "wrong service id",
			args: args{
				req: &StartCollectingRequest{
					ServiceId: "wrongServiceID",
				},
			},
			wantResp: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				err, _ := i1.(error)

				return assert.ErrorContains(t, err, ErrInvalidServiceID.Error())
			},
			wantErr: true,
		},
		{
			name: "evalManager missing",
			args: args{
				req: &StartCollectingRequest{
					ServiceId:   "00000000-0000-0000-0000-000000000000",
					EvalManager: "",
				},
			},
			wantResp: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				err, _ := i1.(error)

				return assert.Equal(t, err, ErrMissingEvalManager)
			},
			wantErr: true,
		},
		{
			name: "configuration serviceId missing",
			args: args{
				req: &StartCollectingRequest{
					ServiceId:   "00000000-0000-0000-0000-000000000000",
					EvalManager: "some-evalManager",
					Configuration: &ServiceConfiguration{
						ServiceId:        "",
						CollectionModule: ServiceConfiguration_WORKLOAD_CONFIGURATION,
						RawConfiguration: &ServiceConfiguration_WorkloadSecurityConfig{},
					},
				},
			},
			wantResp: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				err, _ := i1.(error)
				return assert.ErrorContains(t, err, ErrMissingServiceConfigurationWorkloadServiceIDM.Error())
			},
			wantErr: true,
		},
		{
			name: "raw configurationmissing",
			args: args{
				req: &StartCollectingRequest{
					ServiceId:   "00000000-0000-0000-0000-000000000000",
					EvalManager: "some-evalManager",
					Configuration: &ServiceConfiguration{
						ServiceId:        "",
						CollectionModule: ServiceConfiguration_WORKLOAD_CONFIGURATION,
					},
				},
			},
			wantResp: func(tt assert.TestingT, i1 interface{}, i2 ...interface{}) bool {
				err, _ := i1.(error)

				return assert.ErrorContains(t, err, ErrMissingServiceConfiguration.Error())
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var err error
			if err = tt.args.req.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.wantResp != nil {
				tt.wantResp(t, err)
			}
		})
	}
}

func TestStartCollectingRequest_KubeConfig(t *testing.T) {
	type fields struct {
		ServiceId     string
		MetricId      string
		EvalManager   string
		Configuration *ServiceConfiguration
	}
	tests := []struct {
		name       string
		fields     fields
		wantConfig []byte
		wantErr    assert.ErrorAssertionFunc
	}{
		{
			name: "Empty configuration",
			fields: fields{
				ServiceId:     "00000000-0000-0000-0000-000000000000",
				Configuration: &ServiceConfiguration{},
			},
			wantConfig: nil,
			wantErr: func(tt assert.TestingT, err error, i2 ...interface{}) bool {
				return assert.Contains(t, err.Error(), "configuration not available:")
			},
		},
		{
			name: "Wrong configuration type",
			fields: fields{
				ServiceId: "00000000-0000-0000-0000-000000000000",
				Configuration: &ServiceConfiguration{
					RawConfiguration: &ServiceConfiguration_AuthenticationSecurityConfig{},
				},
			},
			wantConfig: nil,
			wantErr: func(tt assert.TestingT, err error, i2 ...interface{}) bool {
				return assert.ErrorIs(t, err, ErrInvalidWorkloadConfigurationRawConfiguration)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := &StartCollectingRequest{
				ServiceId:     tt.fields.ServiceId,
				MetricId:      tt.fields.MetricId,
				EvalManager:   tt.fields.EvalManager,
				Configuration: tt.fields.Configuration,
			}
			got, err := req.KubeConfig()
			assert.Equal(t, got, tt.wantConfig)
			if tt.wantErr != nil {
				tt.wantErr(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
