// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";

package cam;

import "api/assessment/metric.proto";
import "api/collection/collection.proto";
import "api/orchestrator/orchestrator.proto";
import "google/api/annotations.proto";
import "google/protobuf/empty.proto";

option go_package = "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration";

// This service represents the GX CAM Configuration Interface
service Configuration {
  rpc StartMonitoring(StartMonitoringRequest)
      returns (StartMonitoringResponse) {
    option (google.api.http) = {
      post : "/v1/configuration/monitoring/{service_id}/start"
      body : "*"
    };
  }

  rpc StopMonitoring(StopMonitoringRequest) returns (StopMonitoringResponse) {
    option (google.api.http) = {
      post : "/v1/configuration/monitoring/{service_id}/stop"
    };
  }

  rpc GetMonitoringStatus(GetMonitoringStatusRequest)
      returns (MonitoringStatus) {
    option (google.api.http) = {
      get : "/v1/configuration/monitoring/{service_id}"
    };
  }

  // Lists metrics. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc ListMetrics(clouditor.ListMetricsRequest)
      returns (clouditor.ListMetricsResponse) {
    option (google.api.http) = {
      get : "/v1/configuration/metrics"
    };
  }

  // Retrieves a particular metric. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc GetMetric(clouditor.GetMetricRequest) returns (clouditor.Metric) {
    option (google.api.http) = {
      get : "/v1/configuration/metrics/{metric_id}"
    };
  }

  // Registers a new target cloud service. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc RegisterCloudService(clouditor.RegisterCloudServiceRequest)
      returns (clouditor.CloudService) {
    option (google.api.http) = {
      post : "/v1/configuration/cloud_services"
      body : "service"
    };
  }

  // Registers a new target cloud service. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc UpdateCloudService(clouditor.UpdateCloudServiceRequest)
      returns (clouditor.CloudService) {
    option (google.api.http) = {
      put : "/v1/configuration/cloud_services/{service_id}"
      body : "service"
    };
  }

  rpc ConfigureCloudService(ConfigureCloudServiceRequest)
      returns (ConfigureCloudServiceResponse) {
    option (google.api.http) = {
      put : "/v1/configuration/cloud_services/{service_id}/configurations"
      body : "configurations"
    };
  }

  // Retrieves a target cloud service. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc GetCloudService(clouditor.GetCloudServiceRequest)
      returns (clouditor.CloudService) {
    option (google.api.http) = {
      get : "/v1/configuration/cloud_services/{service_id}"
    };
  }

  // Lists all target cloud services. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc ListCloudServices(clouditor.ListCloudServicesRequest)
      returns (clouditor.ListCloudServicesResponse) {
    option (google.api.http) = {
      get : "/v1/configuration/cloud_services"
    };
  }

  // Removes a target cloud service. This function is a wrapper around the
  // Clouditor Orchestrator's identical function, but we explicitly specify it
  // here so that we can expose it via the configuration interfaces's REST API.
  rpc RemoveCloudService(clouditor.RemoveCloudServiceRequest)
      returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete : "/v1/orchestrator/cloud_services/{service_id}"
    };
  }

  rpc ListControls(clouditor.ListRequirementsRequest)
      returns (clouditor.ListRequirementsResponse) {
    option (google.api.http) = {
      get : "/v1/configuration/controls"
    };
  }

  rpc ListCollectionModules(ListCollectionModulesRequest)
      returns (ListCollectionModulesResponse);
  rpc AddCollectionModule(AddCollectionModuleRequest)
      returns (CollectionModule);
  rpc FindCollectionModule(FindCollectionModuleRequest)
      returns (CollectionModule);
  rpc RemoveCollectionModule(RemoveCollectionModuleRequest)
      returns (google.protobuf.Empty);
}

message StartMonitoringRequest {
  string service_id = 1;
  repeated string control_ids = 2;
}
message StartMonitoringResponse {}

message StopMonitoringRequest { string service_id = 1; }
message StopMonitoringResponse {}

message GetMonitoringStatusRequest { string service_id = 1; }

message GetMetricRequest { int32 metric_id = 1; }

message GetControlRequest {
  string service_id = 1;
  string control_id = 2;
}

message ListCollectionModulesRequest {}
message ListCollectionModulesResponse { repeated CollectionModule modules = 1; }

message AddCollectionModuleRequest { CollectionModule module = 1; }

message FindCollectionModuleRequest { string metric_id = 1; }

message RemoveCollectionModuleRequest { string module_id = 1; }

message ConfigureCloudServiceRequest {
  string service_id = 1;
  Configurations configurations = 2;
}
message ConfigureCloudServiceResponse {}

message Configurations { repeated ServiceConfiguration configurations = 1; }

message MonitoringStatus {
  string service_id = 1;
  repeated string control_ids = 2;
}
