// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection/authsec"
)

const DefaultPort = 50051

var (
	log  = logrus.WithField("service", "collection-authsec")
	port = flag.Int("port", DefaultPort, "The port to bind to")
)

func main() {
	log.Println("Authentication Security: starting up server...")

	// Listen on specified port
	lis, err := net.Listen("tcp", fmt.Sprintf(":%v", *port))
	if err != nil {
		log.Fatalf("Authentication Security: failed to listen: %v", err)
	}

	// Create gRPC Server (srv) and register the auth service (svc) on it
	srv := grpc.NewServer()
	svc := authsec.NewServer()
	collection.RegisterCollectionServer(srv, svc)

	// Enable reflection, primary for testing in early stages
	reflection.Register(srv)

	// Start server (blocks until process is killed or stopped)
	log.Infof("Starting gRPC server for Authentication Security CM on port: %d", port)
	if err = srv.Serve(lis); err != nil {
		log.Fatalf("Authentication Security: failed to serve: %v", err)
	}
}
