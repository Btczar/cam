// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"net"
	"os"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/config"
	serviceEvaluation "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/evaluation"

	"clouditor.io/clouditor/logging/formatter"
	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/persistence/gorm"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	log   *logrus.Entry
	db    persistence.Storage
	types = []any{evaluation.EvaluationResult{},
		evaluation.Compliance{}}
)

const (
	ConfigurationServiceAddressFlag = "configuration-service-address"
	DBUserFlag                      = "db-user"
	DBPasswordFlag                  = "db-password"
	DBHostFlag                      = "db-host"
	DBPortFlag                      = "db-port"
	DBNameFlag                      = "db-name"
	DBInMemoryFlag                  = "db-in-memory"
	APIgRPCPortFlag                 = "api-grpc-port"

	DefaultConfigurationServiceAddress = "localhost:50100"
	DefaultAPIgRPCPort                 = 50101
	DefaultDBUser                      = "postgres"
	DefaultDBPassword                  = "postgres"
	DefaultDBHost                      = "localhost"
	DefaultDBPort                      = 5432
	DefaultDBName                      = "postgres"
	DefaultInMemory                    = false

	EnvPrefix = "CAM"
)

func init() {
	log = logrus.WithField("component", "eval-manager")
	log.Logger.Formatter = formatter.CapitalizeFormatter{Formatter: &logrus.TextFormatter{ForceColors: true}}

	cobra.OnInitialize(config.InitConfig)
}

func newEvalManagerCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "cam-eval-manager",
		Short: "cam-eval-manager launches the CAM Evaluation Manager",
		Long:  "The CAM Evaluation Manager takes care of evaluating evidence gathered by the collection modules.",
		RunE:  doCmd,
	}

	cmd.Flags().String(ConfigurationServiceAddressFlag, DefaultConfigurationServiceAddress, "Specifies the address of the configuration service (cam-req-manager)")
	cmd.Flags().String(DBUserFlag, DefaultDBUser, "Specifies the username of the database")
	cmd.Flags().String(DBPasswordFlag, DefaultDBPassword, "Specifies the password of the database")
	cmd.Flags().String(DBHostFlag, DefaultDBHost, "Specifies the hostname of the database")
	cmd.Flags().Uint16(DBPortFlag, DefaultDBPort, "Specifies the port of the database")
	cmd.Flags().String(DBNameFlag, DefaultDBName, "Specifies the name of the database")
	cmd.Flags().Bool(DBInMemoryFlag, DefaultInMemory, "Specifies whether to use an in-memory database")
	cmd.Flags().Uint16(APIgRPCPortFlag, DefaultAPIgRPCPort, "Specifies the port used by the gRPC API")

	_ = viper.BindPFlag(ConfigurationServiceAddressFlag, cmd.Flags().Lookup(ConfigurationServiceAddressFlag))
	_ = viper.BindPFlag(DBUserFlag, cmd.Flags().Lookup(DBUserFlag))
	_ = viper.BindPFlag(DBPasswordFlag, cmd.Flags().Lookup(DBPasswordFlag))
	_ = viper.BindPFlag(DBHostFlag, cmd.Flags().Lookup(DBHostFlag))
	_ = viper.BindPFlag(DBPortFlag, cmd.Flags().Lookup(DBPortFlag))
	_ = viper.BindPFlag(DBNameFlag, cmd.Flags().Lookup(DBNameFlag))
	_ = viper.BindPFlag(DBInMemoryFlag, cmd.Flags().Lookup(DBInMemoryFlag))
	_ = viper.BindPFlag(APIgRPCPortFlag, cmd.Flags().Lookup(APIgRPCPortFlag))

	return cmd
}

func doCmd(cmd *cobra.Command, args []string) (err error) {
	// Check for in-memory database
	if viper.GetBool(DBInMemoryFlag) {
		db, err = gorm.NewStorage(
			gorm.WithInMemory(),
			gorm.WithAdditionalAutoMigration(types...),
			gorm.WithMaxOpenConns(1),
		)
	} else {
		log.Infof("Connecting to storage %s@%s:%d/%s",
			viper.GetString(DBUserFlag),
			viper.GetString(DBHostFlag),
			viper.GetInt32(DBPortFlag),
			viper.GetString(DBNameFlag))

		db, err = gorm.NewStorage(
			gorm.WithPostgres(
				viper.GetString(DBHostFlag),
				uint16(viper.GetUint(DBPortFlag)),
				viper.GetString(DBUserFlag),
				viper.GetString(DBPasswordFlag),
				viper.GetString(DBNameFlag),
				"require",
			),
			gorm.WithAdditionalAutoMigration(types...),
		)
	}
	if err != nil {
		return fmt.Errorf("could not create storage: %w", err)
	}

	port := viper.GetInt32(APIgRPCPortFlag)

	// Specify port for client requests
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		return err
	}

	// Create gRPC Server (srv) and register the evaluation service (svc) on it
	srv := grpc.NewServer()
	svc := serviceEvaluation.NewServer(
		serviceEvaluation.WithStorage(db),
		serviceEvaluation.WithRequirementsManagerAddress(viper.GetString(ConfigurationServiceAddressFlag)))
	// TODO(lebogg): Remove
	svc.ScheduleComplianceCheck("SomeService", "SomeControl")
	evaluation.RegisterEvaluationServer(srv, svc)

	// Enable reflection, primary for testing in early stages
	reflection.Register(srv)

	// Start to serve (blocks until process is killed or stopped)
	log.Infof("Starting gRPC server for Evaluation Manager on port: %d", port)
	if err = srv.Serve(lis); err != nil {
		log.Errorf("Failed to serve: %v", err)
		return err
	}

	return nil
}

func main() {
	var cmd = newEvalManagerCommand()

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
