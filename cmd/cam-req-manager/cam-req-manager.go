// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"os"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/configuration"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/config"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/strct"
	service_configuration "gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/configuration"

	"clouditor.io/clouditor/api/orchestrator"
	"clouditor.io/clouditor/logging/formatter"
	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/persistence/gorm"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	log   *logrus.Entry
	db    persistence.Storage
	types = []any{collection.CollectionModule{}}
)

const (
	EvaluationServiceAddressFlag = "evaluation-service-address"
	DBUserFlag                   = "db-user"
	DBPasswordFlag               = "db-password"
	DBHostFlag                   = "db-host"
	DBPortFlag                   = "db-port"
	DBNameFlag                   = "db-name"
	DBInMemoryFlag               = "db-in-memory"
	APIgRPCPortFlag              = "api-grpc-port"

	// DefaultEvaluationServiceAddress sets the default target address (evaluation) for the collection modules
	DefaultEvaluationServiceAddress = "localhost:50101"

	DefaultDBUser     = "postgres"
	DefaultDBPassword = "postgres"
	DefaultDBHost     = "localhost"
	DefaultDBPort     = 5432
	DefaultDBName     = "postgres"
	DefaultInMemory   = false

	// defaultGRPCPort sets the default port for the requirements manager
	DefaultAPIgRPCPort = 50100

	// // defaultAuthSecCMAddress sets the default target address for the Authentication Security Test Collection Module
	// defaultAuthSecCMAddress = "auth-sec:50051"
	// defaultRemoteIntegrityCMAddress sets the default target address for the Remote Integrity Collection Module
	defaultRemoteIntegrityCMAddress = "ricm:50052"
	// // DefaultWorkloadCollectionAddress sets the default target address for Workload Configuration Collection Module
	// defaultWorkloadCMAddress = "collection-workload:50053"

	// Test Collection Module
	defaultAuthSecCMAddress = "localhost:50051"
	// DefaultWorkloadCollectionAddress sets the default target address for Workload Configuration Collection Module
	defaultWorkloadCMAddress = "localhost:50053"

	defaultKubeConfigPath = "kube.yaml"

	// For testing we use a hardcoded UUID
	someUUID = "b68549ac-ae08-423b-9a60-ec0646483224"
)

func init() {
	log = logrus.WithField("component", "req-manager")
	log.Logger.Formatter = formatter.CapitalizeFormatter{Formatter: &logrus.TextFormatter{ForceColors: true}}

	cobra.OnInitialize(config.InitConfig)
}

func newReqManagerCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "cam-req-manager",
		Short: "cam-req-manager launches the CAM Requirements Manager",
		Long:  "The CAM Requirements Manager takes care of configuring everything.",
		RunE:  doCmd,
	}

	cmd.Flags().String(EvaluationServiceAddressFlag, DefaultEvaluationServiceAddress, "Specifies the address of the evaluation service (cam-eval-manager)")
	cmd.Flags().String(DBUserFlag, DefaultDBUser, "Specifies the username of the database")
	cmd.Flags().String(DBPasswordFlag, DefaultDBPassword, "Specifies the password of the database")
	cmd.Flags().String(DBHostFlag, DefaultDBHost, "Specifies the hostname of the database")
	cmd.Flags().Uint16(DBPortFlag, DefaultDBPort, "Specifies the port of the database")
	cmd.Flags().String(DBNameFlag, DefaultDBName, "Specifies the name of the database")
	cmd.Flags().Bool(DBInMemoryFlag, DefaultInMemory, "Specifies whether to use an in-memory database")
	cmd.Flags().Uint16(APIgRPCPortFlag, DefaultAPIgRPCPort, "Specifies the port used by the gRPC API")

	_ = viper.BindPFlag(EvaluationServiceAddressFlag, cmd.Flags().Lookup(EvaluationServiceAddressFlag))
	_ = viper.BindPFlag(DBUserFlag, cmd.Flags().Lookup(DBUserFlag))
	_ = viper.BindPFlag(DBPasswordFlag, cmd.Flags().Lookup(DBPasswordFlag))
	_ = viper.BindPFlag(DBHostFlag, cmd.Flags().Lookup(DBHostFlag))
	_ = viper.BindPFlag(DBPortFlag, cmd.Flags().Lookup(DBPortFlag))
	_ = viper.BindPFlag(DBNameFlag, cmd.Flags().Lookup(DBNameFlag))
	_ = viper.BindPFlag(DBInMemoryFlag, cmd.Flags().Lookup(DBInMemoryFlag))
	_ = viper.BindPFlag(APIgRPCPortFlag, cmd.Flags().Lookup(APIgRPCPortFlag))

	return cmd
}

func doCmd(cmd *cobra.Command, args []string) (err error) {
	log.Println("This is GXFS's Requirements Manager")

	// Check for in-memory database
	if viper.GetBool(DBInMemoryFlag) {
		db, err = gorm.NewStorage(
			gorm.WithInMemory(),
			gorm.WithAdditionalAutoMigration(types...),
			gorm.WithMaxOpenConns(1),
		)
	} else {
		log.Infof("Connecting to storage %s@%s:%d/%s",
			viper.GetString(DBUserFlag),
			viper.GetString(DBHostFlag),

			viper.GetInt32(DBPortFlag),
			viper.GetString(DBNameFlag))

		db, err = gorm.NewStorage(
			gorm.WithPostgres(
				viper.GetString(DBHostFlag),
				uint16(viper.GetUint(DBPortFlag)),
				viper.GetString(DBUserFlag),
				viper.GetString(DBPasswordFlag),
				viper.GetString(DBNameFlag),
				"require",
			),
			gorm.WithAdditionalAutoMigration(types...),
		)
	}
	if err != nil {
		return fmt.Errorf("could not create storage: %w", err)
	}

	port := viper.GetInt32(APIgRPCPortFlag)

	sock, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("could not listen: %v", err)
	}

	// Create gRPC Server (srv) and register configuration service (svc) on it - including the orchestrator
	srv := grpc.NewServer()
	svc := service_configuration.NewServer(
		service_configuration.WithStorage(db),
		service_configuration.WithEvalManagerAddress(viper.GetString(EvaluationServiceAddressFlag)),
	)
	configuration.RegisterConfigurationServer(srv, svc)
	orchestrator.RegisterOrchestratorServer(srv, svc)

	// Enable reflection, primary for testing in early stages
	reflection.Register(srv)

	// TODO(lebogg): For testing purposes, collection modules are added here right now.
	// TODO(anatheka): Missing communication security module in addCollectionModules()
	// In the future, triggered via REST calls.
	log.Info("Adding collection modules...")
	addCollectionModules(svc)

	// In the future, triggered via REST calls.
	log.Info("Configure cloud services...")
	configureCloudServices(svc)

	// Start Monitoring Control "CKM-03" for Kubernetes
	control := "CKM-03"
	serviceID := someUUID
	log.Infof("Start monitoring control %s for service %s ...", control, serviceID)
	_, err = svc.StartMonitoring(context.TODO(), &configuration.StartMonitoringRequest{
		ServiceId:  serviceID,
		ControlIds: []string{control},
	})
	if err != nil {
		log.Errorf("Couldn't start monitoring the control %s for service %s: %v", control, serviceID, err)
	}

	// Start Monitoring Control "CKM-03" for Openstack
	control = "CKM-03"
	serviceID = someUUID
	log.Infof("Start monitoring control %s for service %s ...", control, serviceID)
	_, err = svc.StartMonitoring(context.TODO(), &configuration.StartMonitoringRequest{
		ServiceId:  serviceID,
		ControlIds: []string{control},
	})
	if err != nil {
		log.Errorf("Couldn't start monitoring the control %s for service %s: %v", control, serviceID, err)
	}

	// Start Monitoring Control "IAM-07"
	control = "IAM-07"
	serviceID = someUUID
	log.Infof("Start monitoring control %s for service %s ...", control, serviceID)
	_, err = svc.StartMonitoring(context.TODO(), &configuration.StartMonitoringRequest{
		ServiceId:  serviceID,
		ControlIds: []string{control},
	})
	if err != nil {
		log.Errorf("Couldn't start monitoring the control %s for service %s: %v", control, serviceID, err)
	}

	// Start Monitoring Control "OPS-21"
	control = "OPS-21"
	serviceID = someUUID
	log.Infof("Start monitoring control %s for service %s ...", control, serviceID)
	_, err = svc.StartMonitoring(context.TODO(), &configuration.StartMonitoringRequest{
		ServiceId:  serviceID,
		ControlIds: []string{control},
	})
	if err != nil {
		log.Errorf("Couldn't start monitoring the control %s for service %s: %v", control, serviceID, err)
		return err
	}

	log.Infof("Starting gRPC server for Requirements Manager on port %d ...", port)
	if err = srv.Serve(sock); err != nil {
		log.Fatalf("Failed to serve gRPC endpoint: %s", err)
		return err
	}

	return nil
}

func configureCloudServices(svc configuration.ConfigurationServer) {
	var (
		err     error
		configs []*collection.ServiceConfiguration
	)

	// Configure Kubernetes
	c, err := configureWCCM()
	if err != nil {
		err = fmt.Errorf("could not configure service '%s': %w",
			collection.ServiceConfiguration_WORKLOAD_CONFIGURATION, err)
		log.Error(err)
	} else {
		configs = append(configs, c)
	}

	// Configure
	c, err = configureRICM()
	if err != nil {
		err = fmt.Errorf("could not configure service '%s': %w",
			collection.ServiceConfiguration_REMOTE_INTEGRITY, err)
		log.Error(err)
	} else {
		configs = append(configs, c)
	}

	// Configure
	c, err = configureASCM()
	if err != nil {
		err = fmt.Errorf("could not configure service '%s': %w",
			collection.ServiceConfiguration_AUTHENTICATION_SECURITY, err)
		log.Error(err)
	} else {
		configs = append(configs, c)
	}

	_, err = svc.ConfigureCloudService(context.TODO(), &configuration.ConfigureCloudServiceRequest{
		ServiceId: someUUID,
		Configurations: &configuration.Configurations{
			Configurations: configs},
	})
	if err != nil {
		log.Error(err)
	}
}

func configureWCCM() (serviceConfiguration *collection.ServiceConfiguration, err error) {
	// Read kube config file and convert the content as protobuf to the configuration
	// It is not possible to store rest.Config as protobuf, because transport.WrapperFunc is not supported
	// TODO(anatheka): For now, we read the kube config from the path until we get the kube config content from the "Dashboard".
	kubeConfig, err := ioutil.ReadFile(defaultKubeConfigPath)
	if err != nil {
		err = fmt.Errorf("could not read kube config file: %w", err)
		return
	}

	kubernetesConfig, err := strct.ToValue(string(kubeConfig))
	if err != nil {
		err = fmt.Errorf("could not convert kube config file to structpb.Value: %w", err)
		return
	}

	rawConfiguration := &collection.ServiceConfiguration_WorkloadSecurityConfig{
		WorkloadSecurityConfig: &collection.WorkloadSecurityConfig{
			Openstack:  nil,
			Kubernetes: kubernetesConfig,
		},
	}

	if err != nil {
		err = fmt.Errorf("could not convert kube config file to structpb.Value: %w", err)
		return
	}

	serviceConfiguration = &collection.ServiceConfiguration{
		ServiceId:        someUUID,
		CollectionModule: collection.ServiceConfiguration_WORKLOAD_CONFIGURATION,
		RawConfiguration: rawConfiguration,
	}
	// If err not nil, directly return it since this fct is similar is like a wrapper of ConfigureCloudService
	return

}

func configureRICM() (serviceConfiguration *collection.ServiceConfiguration, err error) {
	rawConfiguration := &collection.ServiceConfiguration_RemoteIntegrityConfig{
		RemoteIntegrityConfig: &collection.RemoteIntegrityConfig{
			// For testing, insert URL+PORT of VM here
			Target: "TODO",
			Certificate: "-----BEGIN CERTIFICATE-----\nMIICSDCCAc2gAwIBAgIUHxAyr1Y3QlrYutGU317Uy5FhdpQwCgYIKoZIzj0EAwMw" +
				"\nYzELMAkGA1UEBhMCREUxETAPBgNVBAcTCEdhcmNoaW5nMRkwFwYDVQQKExBGcmF1\nbmhvZmVyIEFJU0VDMRAwDgYDVQQLEwdSb2" +
				"90IENBMRQwEgYDVQQDEwtJRFMgUm9v\\ndCBDQTAeFw0yMjA0MDQxNTE3MDBaFw0yNzA0MDMxNTE3MDBaMGMxCzAJBgNVBAYT\\nAk" +
				"RFMREwDwYDVQQHEwhHYXJjaGluZzEZMBcGA1UEChMQRnJhdW5ob2ZlciBBSVNF\\nQzEQMA4GA1UECxMHUm9vdCBDQTEUMBIGA1UEA" +
				"xMLSURTIFJvb3QgQ0EwdjAQBgcq\\nhkjOPQIBBgUrgQQAIgNiAAQSneAVxZRShdfwEu3HtCcwRnV5b4UtOnxJaVZ/bILS\\n4dThZ" +
				"VWpXNm+ikvp6Sk0RlI30mKl2X7fX8aRew+HvvFT08xJw9dGAkm2Fsp+4/c7\\nM3rMhiHXyCpu/Xg4OlxAYOajQjBAMA4GA1UdDwEB" +
				"/wQEAwIBBjAPBgNVHRMBAf8E\\nBTADAQH/MB0GA1UdDgQWBBTyFTqqlt0/YxJBiCB3WM7lkpqWVjAKBggqhkjOPQQD\nAwNpADBmA" +
				"jEAizrjlmYQmrMbsEaGaFzouMT02iMu0NLILhm1wkfAl3UUWymcliy8\nf1IAI1nO4448AjEAkd74w4WEaTqvslmkPktxNhDA1cVL5" +
				"5LDLbUNXLcSdzr2UBhp\nK8Vv1j4nATtg1Vkf\n-----END CERTIFICATE-----",
		},
	}

	serviceConfiguration = &collection.ServiceConfiguration{
		ServiceId:        someUUID,
		CollectionModule: collection.ServiceConfiguration_REMOTE_INTEGRITY,
		RawConfiguration: rawConfiguration,
	}

	// If err not nil, directly return it since this fct is similar is like a wrapper of ConfigureCloudService
	return
}

func configureASCM() (serviceConfiguration *collection.ServiceConfiguration, err error) {
	rawConfiguration := &collection.ServiceConfiguration_AuthenticationSecurityConfig{
		AuthenticationSecurityConfig: &collection.AuthenticationSecurityConfig{
			Issuer:           "https://as.aisec.fraunhofer.de/auth",
			MetadataDocument: "",
		},
	}

	serviceConfiguration = &collection.ServiceConfiguration{
		ServiceId:        someUUID,
		CollectionModule: collection.ServiceConfiguration_AUTHENTICATION_SECURITY,
		RawConfiguration: rawConfiguration,
	}

	// If err not nil, directly return it since this fct is similar is like a wrapper of ConfigureCloudService
	return
}

func addCollectionModules(svc configuration.ConfigurationServer) {
	var (
		err                  error
		collectionModuleName string
	)

	// Add Workload Configuration Collection Module
	collectionModuleName = collection.ServiceConfiguration_WORKLOAD_CONFIGURATION.String()
	_, err = svc.AddCollectionModule(context.TODO(), &configuration.AddCollectionModuleRequest{
		Module: &collection.CollectionModule{
			Id:          collectionModuleName,
			Name:        collectionModuleName,
			Description: "Testing " + collectionModuleName + " Module",
			MetricId:    "AtRestEncryption",
			Address:     defaultWorkloadCMAddress,
		},
	})
	if err != nil {
		log.Errorf("Couldn't add `%s` collection module: %v", collectionModuleName, err)
	} else {
		log.Infof("Added `%s` collection module", collectionModuleName)
	}

	// Add Authentication Security Test Collection Module
	collectionModuleName = collection.ServiceConfiguration_AUTHENTICATION_SECURITY.String()
	_, err = svc.AddCollectionModule(context.TODO(), &configuration.AddCollectionModuleRequest{
		Module: &collection.CollectionModule{
			Id:          collectionModuleName,
			Name:        collectionModuleName,
			Description: "Testing " + collectionModuleName + " Module",
			MetricId:    "OAuthGrantTypes",
			Address:     defaultAuthSecCMAddress,
		},
	})
	if err != nil {
		log.Errorf("Couldn't add `%s` collection module: %v", collectionModuleName, err)
	} else {
		log.Infof("Added `%s` collection module", collectionModuleName)
	}

	// Add Remote Integrity Collection Module
	collectionModuleName = collection.ServiceConfiguration_REMOTE_INTEGRITY.String()
	_, err = svc.AddCollectionModule(context.TODO(), &configuration.AddCollectionModuleRequest{
		Module: &collection.CollectionModule{
			Id:          collectionModuleName,
			Name:        collectionModuleName,
			Description: "Testing " + collectionModuleName + " Module",
			MetricId:    "SystemComponentsIntegrity",
			Address:     defaultRemoteIntegrityCMAddress,
		},
	})
	if err != nil {
		log.Errorf("Couldn't add `%s` collection module: %v", collectionModuleName, err)
	} else {
		log.Infof("Added `%s` collection module", collectionModuleName)
	}
}

func main() {
	var cmd = newReqManagerCommand()

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
