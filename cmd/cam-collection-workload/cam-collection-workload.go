// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"net"

	"clouditor.io/clouditor/logging/formatter"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/service/collection/workload"
)

var (
	log *logrus.Entry
)

func init() {
	log = logrus.WithField("component", "collection-workload")
	log.Logger.Formatter = formatter.CapitalizeFormatter{Formatter: &logrus.TextFormatter{ForceColors: true}}
}

const (
	DefaultGrpcPort = 50053
)

func main() {
	log.Logger.Formatter = formatter.CapitalizeFormatter{Formatter: &logrus.TextFormatter{ForceColors: true}}
	log.Info("Start Workload Configuration...")

	// create a new socket for gRPC communication
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", DefaultGrpcPort))
	if err != nil {
		log.Errorf("could not listen: %v", err)
	}

	// Create gRPC Server (srv) and register workload service (svc) on it
	srv := grpc.NewServer()
	svc := workload.NewServer()
	collection.RegisterCollectionServer(srv, svc)

	// Enable reflection, primary for testing in early stages
	reflection.Register(srv)

	// Start server (blocks until process is killed or stopped)
	log.Infof("Starting gRPC server for Workload Configuration CM on port: %d", DefaultGrpcPort)
	if err = srv.Serve(lis); err != nil {
		log.Fatalf("Authentication Security: failed to serve: %v", err)
	}
}
