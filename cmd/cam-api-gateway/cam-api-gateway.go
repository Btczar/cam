// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/config"

	"clouditor.io/clouditor/logging/formatter"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var log *logrus.Entry

const (
	ConfigurationServiceAddressFlag = "configuration-service-address"
	EvaluationServiceAddressFlag    = "evaluation-service-address"

	DefaultConfigurationServiceAddress = "localhost:50100"
	DefaultEvaluationServiceAddress    = "localhost:50101"
)

func init() {
	log = logrus.WithField("component", "grpc-gateway")
	log.Logger.Formatter = formatter.CapitalizeFormatter{Formatter: &logrus.TextFormatter{ForceColors: true}}

	cobra.OnInitialize(config.InitConfig)
}

func newGatewayCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "cam-api-gateway",
		Short: "cam-api-gateway launches the CAM API Gateway",
		Long:  "The CAM API Gateway serves as central gateway to expose the functionality of different CAM micro-service using a central REST API.",
		RunE:  doCmd,
	}

	cmd.Flags().String(ConfigurationServiceAddressFlag, DefaultConfigurationServiceAddress, "Specifies the address of the configuration service (cam-req-manager)")
	cmd.Flags().String(EvaluationServiceAddressFlag, DefaultEvaluationServiceAddress, "Specifies the address of the evaluation service (cam-eval-manager)")

	_ = viper.BindPFlag(ConfigurationServiceAddressFlag, cmd.Flags().Lookup(ConfigurationServiceAddressFlag))
	_ = viper.BindPFlag(EvaluationServiceAddressFlag, cmd.Flags().Lookup(EvaluationServiceAddressFlag))

	return cmd
}

func doCmd(cmd *cobra.Command, args []string) error {
	err := cam.RunGateway(
		viper.GetString(ConfigurationServiceAddressFlag),
		viper.GetString(EvaluationServiceAddressFlag),
		8080, log)
	if err != nil {
		return fmt.Errorf("could not start gRPC-Gateway for REST: %w", err)
	}

	return nil
}

func main() {
	var cmd = newGatewayCommand()

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
