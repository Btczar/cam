package strct

import (
	"encoding/json"
	"fmt"

	"google.golang.org/protobuf/types/known/structpb"
)

func ToValue(strct any) (value *structpb.Value, err error) {
	var b []byte

	value = new(structpb.Value)

	if b, err = json.Marshal(strct); err != nil {
		err = fmt.Errorf("JSON marshal failed: %w", err)
		return
	}
	if err = json.Unmarshal(b, &value); err != nil {
		err = fmt.Errorf("JSON unmarshal failed: %w", err)
		return
	}
	return
}

func ToStruct[T any](value *structpb.Value) (strct T, err error) {
	var b []byte

	if value == nil {
		err = fmt.Errorf("empty value")
		return
	}

	if b, err = json.Marshal(value); err != nil {
		err = fmt.Errorf("JSON marshal failed: %w", err)
		return
	}
	if err = json.Unmarshal(b, &strct); err != nil {
		err = fmt.Errorf("JSON unmarshal failed: %w", err)
		return
	}
	return
}

// ToByteArray converts the protobuf value to a byte array
func ToByteArray(v *structpb.Value) (b []byte, err error) {

	value := v.AsInterface()

	if value == nil {
		err = fmt.Errorf("converting raw configuration to map is empty or nil")
		return
	}

	switch v := value.(type) {
	case string:
		return []byte(v), nil
	default:
		return nil, fmt.Errorf("got type %T but wanted string", v)
	}
}
