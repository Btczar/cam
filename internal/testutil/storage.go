package testutil

import (
	"testing"

	"clouditor.io/clouditor/persistence"
	"clouditor.io/clouditor/persistence/gorm"
	"gorm.io/gorm/logger"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/collection"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/api/evaluation"
)

// NewInMemoryStorage uses the Clouditor inmemory package to create a new in-memory storage that can be used for unit
// testing. The funcs varargs can be used to immediately execute storage operations on it.
func NewInMemoryStorage(t *testing.T, funcs ...func(s persistence.Storage)) (s persistence.Storage) {
	var err error

	s, err = gorm.NewStorage(
		gorm.WithInMemory(),
		gorm.WithMaxOpenConns(1),
		gorm.WithAdditionalAutoMigration(
			evaluation.EvaluationResult{},
			evaluation.Compliance{},
			collection.CollectionModule{}),
		gorm.WithLogger(logger.Default.LogMode(logger.Silent)))
	if err != nil {
		t.Errorf("Could not initialize in-memory db: %v", err)
	}

	for _, f := range funcs {
		f(s)
	}

	return
}
